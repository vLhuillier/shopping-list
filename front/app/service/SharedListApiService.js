
class SharedListApiService extends BaseAPIService{

    constructor() {
        super("listsSharing")
    }

    checkAuthentication(token) {
        if(this.token === null || this.token === undefined && token !== null && token !== undefined){
            this.token = token;
        }
        return fetchJSON(`http://localhost:3333/user/checkAuthentication/${token}`, this.token);
    }

    getAll(userId) {
        return fetchJSON('http://localhost:3333/listsSharingByUser/' + userId, this.token)
    }

    getAllListsArchived(userId) {
        return fetchJSON('http://localhost:3333/listsArchived/' + userId, this.token)
    }

    get(id) {
        return fetchJSON(`${this.url}/${id}`, this.token)
    }

    async delete(sharedList) {

        let i = 0;
        if(sharedList.users && sharedList.users.length !== 0){
            for(const user of sharedList.users) {
                if (i === sharedList.users.length - 1) {
                    return fetch(`http://localhost:3333/listsSharingByUser/${sharedList.list.id}/${user.id}`, {
                        method: 'DELETE',
                        headers: this.headers
                    });
                }
                await fetch(`http://localhost:3333/listsSharingByUser/${sharedList.list.id}/${user.id}`, {
                    method: 'DELETE',
                    headers: this.headers
                });
                i++;
            }

        }

    }

    deleteUserFromSharedList(listId, userId) {
        this.headers.delete('Content-Type')
        return fetch(`http://localhost:3333/listsSharingByUser/${listId}/${userId}`, { method: 'DELETE', headers: this.headers })
    }

    insert(sharedList) {
        this.headers.set( 'Content-Type', 'application/json' );
        return new Promise((resolve, reject) =>
            fetch("http://localhost:3333/listsSharingByUser", {
                method: 'POST',
                headers: this.headers,
                body: JSON.stringify(sharedList)
            }).then(res => {
                resolve(res);
            }).catch(e => reject(e))
        )
    }

    updatePermission(sharedList) {

        sharedList.list_id = sharedList.list.id;
        delete sharedList.users;
        delete sharedList.list;

        this.headers.set( 'Content-Type', 'application/json' )
        return fetch("http://localhost:3333/listsArchived/", {
            method: 'PUT',
            headers: this.headers,
            body: JSON.stringify(sharedList)
        })
    }

}