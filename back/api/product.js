module.exports = (app, productService, jwt) => {

    /* Récupére tous les produits */
    app.get("/product", jwt.validateJWT, async (req, res) => {
        res.json(await productService.dao.getAll());
    });

    /* Récupére tous les produits par liste */
    app.get("/productByList/:id", jwt.validateJWT, async (req, res) => {
        let products = await productService.dao.getAllByListId(req.params.id);
        if(products !== null){
            if(products.length === 0 ){
                return res.status(404).end();
            }
            return res.json(products);
        }
        return res.status(404).end();
    });

    /* Récupére un produit */
    app.get("/product/:id", jwt.validateJWT, async (req, res) => {
        try {
            const product = await productService.dao.getById(req.params.id)
            if (product === undefined || product === null || product.length === 0 ) {
                return res.status(404).end();
            }
            return res.json(product);
        } catch (e) { res.status(400).end() };
    });

    /* Ajoute un produit */
    app.post("/product", jwt.validateJWT, (req, res) => {
        const product = req.body;
        delete product.id;
        if (!productService.isValid(product))  {
            return res.status(400).end();
        }
        productService.dao.insert(product)
            .then( id => {
                product.id = id;
                return res.json(product);

            })
            .catch(e => {
                res.status(500).end();
            });
    });

    /* Supprime un produit */
    app.delete("/product/:id", jwt.validateJWT, async (req, res) => {
        const product = await productService.dao.getById(req.params.id);
        if (product === undefined || product === null || product.length === 0) {
            return res.status(404).end();
        }
        productService.dao.delete(req.params.id)
            .then(res.status(200).end())
            .catch(e => {
                res.status(500).end();
            });
    });

    /* Met à jour un produit */
    app.put("/product", jwt.validateJWT, async (req, res) => {
        let product = req.body;
        let productUpdated = req.body;
        if ((product === undefined) || (product === null) ) {
            return res.status(400).end();
        }
        if ((product.id === undefined) || (product.id === null) || (!productService.isValid(product))) {
            return res.status(400).end();
        }
        product = await productService.dao.getById(product.id);
        if (product === undefined || product.length === 0) {
            return res.status(404).end();
        }
        productService.dao.update(productUpdated)
            .then(res.status(200).end())
            .catch(e => {
                res.status(500).end();
            });
    });
}
