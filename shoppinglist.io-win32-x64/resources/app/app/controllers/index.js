class IndexController extends BaseController {

    constructor() {
        super();
        this.constants = new Constants();

        this.lists;
        this.newList = new List();
        this.editList = new List();
        this.lastDeletedLists = Array();
        this.formMode = 'add';
        this.permission = 'write';

        this.sharedLists;
        this.newSharedList = new SharedList();
        this.editSharedList = new SharedList();
        this.lastDeletedSharedLists = Array();
        this.sharedFormMode = 'add';

        this.displayAllLists();


    }

    /* Fonction communes */
    async displayAllLists() {
        this.constants.LOADER_SELECTOR.style = "display: flex!important;";
        await this.checkAuthentication().then( () => {
            if(this.user !== null && this.user !== undefined ){
                this.constants.MODAL_LOGIN_SELECTOR.style = "display: none!important;opacity: 0!important;";
                document.getElementById('listAddFabButton').style = "display: block!important;";

                this.displayAllPersonnalLists(this.user.id).then( (personnalLists) => {
                    if(personnalLists){
                        this.displayAllSharedLists(this.user.id).then( (sharedLists) => {
                            if(sharedLists){
                                var elems = document.querySelectorAll('.tooltipped');
                                var instances = M.Tooltip.init(elems);
                            }

                            this.constants.LOADER_SELECTOR.style = "display: none!important;";
                        }).catch(e => {
                            this.constants.LOADER_SELECTOR.style = "display: none!important;";
                            this.constants.MODAL_LOGIN_SELECTOR.style = "display: flex!important;opacity: 1!important;";
                        });



                    }

                }).catch(e => {
                    this.constants.LOADER_SELECTOR.style = "display: none!important;";
                    this.constants.MODAL_LOGIN_SELECTOR.style = "display: flex!important;opacity: 1!important;";
                });
            }else{
                this.displayServiceError(401);
                this.constants.LOADER_SELECTOR.style = "display: none!important;";
                this.constants.MODAL_LOGIN_SELECTOR.style = "display: flex!important;opacity: 1!important;";
            }
        }).catch(e => {
            this.constants.LOADER_SELECTOR.style = "display: none!important;";
            this.constants.MODAL_LOGIN_SELECTOR.style = "display: flex!important;opacity: 1!important;";
        });

    }


    /* Fonctions Listes Partagées */
    displayAllSharedLists(){
        const currentDate = new Date();
        currentDate.setHours(0,0,0,0);
        return new Promise((resolve, reject) =>
            this.model.getAllSharedLists(this.user.id).then(sharedLists => {
                let html = "";
                this.sharedLists = sharedLists;

                if(!sharedLists){
                    html = "<h6 class='text-empty-list'>Aucunes Listes Partagée</h6>";
                    document.getElementById('addPulseButton').className = "btn-floating custom-fab-button btn-large custom-bg-color2 pulse tooltipped";
                }else{
                    document.getElementById('addPulseButton').className = "btn-floating custom-fab-button btn-large custom-bg-color2 tooltipped";
                    for(let sharedList of sharedLists) {
                        if( sharedList.list.date < currentDate && !sharedList.list.archived){
                            this.setListArchived(sharedList.list, true);
                        }
                        if(!sharedList.list.archived){
                            let checked;
                            if(sharedList.list.checked){
                                checked = `<input onclick="indexController.switchCheckedStateSharedList(${sharedList.id})" type="checkbox" checked="${checked}" class="filled-in"/>`;
                            }else{
                                checked = `<input onclick="indexController.switchCheckedStateSharedList(${sharedList.id})" type="checkbox" class="filled-in"/>`;
                            }

                            let date = sharedList.list.date ? sharedList.list.date : 'Erreur date';
                            let warning = '';

                            if(sharedList.list.date === currentDate && sharedList.list.checked === false){
                                warning += ` <i class="material-icons orange-text tooltipped warning-list-icon " data-position="top" data-tooltip="Attention: Date de fin aujourd'hui">warning</i>`;
                            }
                            let productsHtml = this.generateLiProductHtml(sharedList.list.products);
                            let usersHtml = this.generateLiUserHtml(sharedList.users);
                            let ownerHtml = this.generateLiUserHtml([sharedList.owner]);
                            html += `<tr id="list${sharedList.id}">
                                        <td class="padding-left-20px">
                                            <p>
                                              <label>
                                              ${warning}
                                                ${checked}
                                                <span></span>
                                              </label>
                                            </p>
                                        </td>
                                        <td>${sharedList.list.shop}</td>
                                        <td>${date.toLocaleDateString()}</td>
                                        <td >${productsHtml}</td>
                                        <td >${usersHtml}</td>
                                        <td >${ownerHtml}</td>`;
                                        if(sharedList.permission === "write" || this.user.id === sharedList.owner.id){
                                            html +=
                                                `<td class="text-align-center">
                                                    <i onclick="indexController.setValuesModalEditSharedList('${sharedList.id}', '${sharedList.list.id}');indexController.modalSharedList('edit', 'write');"
                                                       class="material-icons custom-color pointer tooltipped icon-table" data-position="top" data-tooltip="Editer">
                                                        edit
                                                    </i>`;
                                            if(this.user.id === sharedList.owner.id) {
                                                html += `<i onclick="indexController.modalDeleteSharedList('${sharedList.id}', '${sharedList.list.shop}')"
                                                       class="material-icons orange-text pointer tooltipped icon-table" data-position="top" data-tooltip="Supprimer">
                                                        delete
                                                    </i>`;
                                            }
                                            html += `</td>`;
                                        }else{
                                            html +=
                                                `<td class="text-align-center">
                                                    <i onclick="indexController.setValuesModalEditSharedList('${sharedList.id}', '${sharedList.list.id}');indexController.modalSharedList('edit', 'read');"
                                                       class="material-icons custom-color pointer tooltipped icon-table" data-position="top" data-tooltip="Voir">
                                                        remove_red_eye
                                                    </i>
                                                </td>`;
                                        }
                        }
                    }
                }
                if(this.datatableShared) {
                    this.destroyDataTableShared();
                }
                $('#list_container_shared').innerHTML = html;
                this.initDataTableShared();
                resolve(sharedLists);
            }).catch(e => {this.displayServiceError(e), this.constants.LOADER_SELECTOR.style = "display: none!important;";})
        );
    }

    checkInputStep1Shared(){
        const sharedListNameInput =  this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.value;
        const sharedListDateInput =  this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.value;
        if(sharedListNameInput === '' || sharedListNameInput === null || sharedListNameInput === undefined){
            this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.className = 'validate invalid';
            this.toast('Champ "Nom de la liste" requis', 'warn');
            return;
        }
        if(sharedListDateInput === '' || sharedListDateInput === null || sharedListDateInput === undefined){
            this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.className = 'validate invalid';
            this.toast('Champ "Date" requis', 'warn');
            return;
        }
        if(this.parseDate(sharedListDateInput) == null){
            this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.toast('Champ "Date" invalide', 'warn');
            document.getElementById('listBackButton').className = 'btn-floating btn btn-flat previous-step custom-bg-color2';
            this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.className = 'validate invalid';
            return;
        }
        document.getElementById('sharedListBackButton').className = 'btn-floating btn btn-flat previous-step grey lighten-2';
        this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.className = 'validate valid';
        this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.className = 'validate valid';
        this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.removeAttribute('disabled');
        this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.removeAttribute('disabled');
        this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.removeAttribute('disabled');
        this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.removeAttribute('disabled');

        if(this.permission === 'read'){
            this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_USER_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.SHARED_USER_PERMISSION_INPUT_SELECTOR.setAttribute('disabled','disabled');
            document.getElementById('formsharedUser').style = 'display: none;'
        }else{
            this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.removeAttribute('disabled','disabled');
            this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.removeAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.removeAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.removeAttribute('disabled','disabled');
            this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.removeAttribute('disabled','disabled');
            this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.removeAttribute('disabled','disabled');
            this.constants.SHARED_USER_NAME_INPUT_SELECTOR.removeAttribute('disabled','disabled');
            this.constants.SHARED_USER_PERMISSION_INPUT_SELECTOR.removeAttribute('disabled','disabled');
        }
        if(this.sharedFormMode === 'edit') {
            if(this.editSharedList.owner.id !== this.user.id){
                this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
                document.getElementById('formsharedUser').style = 'display: none;'
            }else{
                this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.removeAttribute('disabled','disabled');
                document.getElementById('formsharedUser').style = 'display: block;'
            }
        }else{
            this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.removeAttribute('disabled','disabled');
            document.getElementById('formsharedUser').style = 'display: block;'
        }

        let date = this.parseDate(sharedListDateInput);
        if(this.sharedFormMode === 'edit') {
            this.editSharedList.list.shop = sharedListNameInput;
            this.editSharedList.list.date = date;
            this.editSharedList.list.archived = false;
            this.editSharedList.list.checked = false;
            this.editSharedList.list.shared = true;
            this.editSharedList.list.user_id = this.user.id;
        }else {
            this.newSharedList.list.shop = sharedListNameInput;
            this.newSharedList.list.date = date.toLocaleString();
            this.newSharedList.list.archived = false;
            this.newSharedList.list.checked = false;
            this.newSharedList.list.shared = true;
            this.newSharedList.list.user_id = this.user.id;
        }

    }

    async modalSharedList(mode, permission = null) {
        this.sharedFormMode = mode;
        if(mode === "edit"){
            this.permission = permission;
        }else{
            this.permission = 'write';
        }
        this.resetStepperFromOtherPage();
        this.formatDatePicker();
        this.resetSharedFormValues();
        this.getModal("#addSharedListModal").open();
        document.getElementById('stepContent1Shared').style.height = 'unset';
        $('#firstStepLiShared').className = "step active";
        $('#secondStepLiShared').className = "step";
        $('#thirdStepLiShared').className = "step";

    }

    async submitAddSharedList() {
        const sharedListName = this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.value;
        if(this.permission === 'write' ) {
            if(this.sharedFormMode === 'edit'){
                if(this.editSharedList.users === null || this.editSharedList.users === undefined || this.editSharedList.users.length === 0){
                    this.getModal("#warningModal").open();
                }else{
                    await this.model.updateList(this.editSharedList.list).then(res => {
                        if(res !== 200 && res !== 304){
                            this.displayServiceError(res);
                        }else{
                            this.displayAllLists();
                            this.toast('Liste Partagée: ' + sharedListName + ' éditée', 'success');
                        }
                    });
                    this.getModal("#addSharedListModal").close();
                    await this.displayAllLists();
                    this.resetSharedFormValues();
                }
            }else{
                if(this.newSharedList.users === null || this.newSharedList.users === undefined || this.newSharedList.users.length === 0){
                    this.getModal("#warningModal").open();
                }else{
                    this.getModal("#addSharedListModal").close();
                    await this.displayAllLists();
                    this.resetSharedFormValues();
                }
            }
        }else{
            this.getModal("#addSharedListModal").close();
        }

    }


    async switchCheckedStateSharedList(index){
        let sharedList =  this.sharedLists.filter(function(item) {
            return item.id === index;
        });

        if(sharedList !== null && sharedList !== undefined && sharedList[0] !== null && sharedList[0] !== undefined){
            sharedList = sharedList[0];
            sharedList.list.checked = !sharedList.list.checked ;
            sharedList.list.date = sharedList.list.date.toLocaleDateString();

            if(sharedList.list.checked){
                this.checkAllProducts(sharedList.list.products);
            }

            await this.model.updateList(sharedList.list).then(res => {
                if(res !== 200 && res !== 304){
                    this.displayServiceError(res);
                }else{
                    this.displayAllLists();
                    this.toast('Liste Partagée "' + sharedList.list.shop + '"  éditée ', 'success');
                }
            });


        }else {
            this.toast(this.constants.HTTP_ERRORS_DEFAULT, 'danger');
        }
        await this.displayAllLists();
    }


    async addSharedProduct(){
        let sharedProductNameInput =  this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.value;
        let sharedQuantityInput =  this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.value;

        if(sharedProductNameInput === '' || sharedProductNameInput === null || sharedProductNameInput === undefined){
            this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.className = 'validate invalid';
            this.toast('Champ "Nom du produit" requis', 'warn');
            return;
        }
        if(sharedQuantityInput === '' || sharedQuantityInput === null || sharedQuantityInput === undefined){
            this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.className = 'validate invalid';
            this.toast('Champ "Quantité" requis', 'warn');
            return;
        }

        const mode = this.sharedFormMode;
        if(mode === 'edit'){
            if(this.permission === "write"){
                let product = new Product(0, false, sharedProductNameInput, sharedQuantityInput, this.editSharedList.list.id);
                await this.model.insertProduct(product).then(res => {
                    if(res !== 200 && res !== 304){
                        this.displayServiceError(res);
                    }else{
                        this.model.getProductsByList(product.list_id).then(products => {
                            this.editSharedList.list.products = products;
                            this.displayAllLists();
                            this.refreshAddSharedProduct();
                            this.toast('Produit "' + sharedProductNameInput + '" ajouté', 'success');
                            this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.value = "";
                            this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.value = "";
                        });
                    }
                });
                product.list_id = this.editSharedList.list.id;
                this.editSharedList.list.products.push(product);
            }else{
                this.displayServiceError("Vous n'êtes pas autorisé à ajouter un produit");
            }

        }else{
            let product = new Product(0, false, sharedProductNameInput, sharedQuantityInput);
            this.newSharedList.list.products.push(product);
            await this.displayAllLists();
            this.refreshAddSharedProduct();
            this.toast('Produit "' + sharedProductNameInput + '" ajouté', 'success');
            this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.value = "";
            this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.value = "";
        }

    }

    refreshAddSharedProduct(){
        let html = '';
        let products;
        if(this.sharedFormMode === 'edit') {
            products = this.editSharedList.list.products;
        }else{
            products = this.newSharedList.list.products;
        }
        let index = 0;
        products.forEach(product => {
            if(this.sharedFormMode === 'add') {
                product.id = index;
            }
            let checked = false;
            if(product.checked){
                checked = `<input onclick="indexController.switchCheckedStateSharedProduct(${product.id}, ${product.checked})" type="checkbox" checked="${checked}" class="filled-in"/>`;
            }else{
                checked = `<input onclick="indexController.switchCheckedStateSharedProduct(${product.id}, ${product.checked})" type="checkbox" class="filled-in"/>`;
            }
            html +=
                '<tr>' +
                '<td>' +
                '<p>' +
                '<label>' +
                checked +
                '<span></span>' +
                '</label>' +
                '</p>' +
                '</td>' +
                '<td>' + product.label + '</td>' +
                '<td class="position-relative"> <div class="round-quantity">' + product.quantity + '</div></td>';
            if(this.permission === "write"){
                html +=
                    '<td class="text-align-center">' +
                    '<i onclick="indexController.modalDeleteSharedProduct('+ product.id +')" ' +
                    'class="material-icons orange-text pointer tooltipped" data-position="right" data-tooltip="Supprimer">' +
                    'delete' +
                    '</i>' +
                    '</td>';

            }else{
                html += '<td></td>';
            }
            html += '<tr>';
            index++;
        });

        this.constants.SHARED_LIST_PRODUCT_CONTAINER_SELECTOR.innerHTML = html;
        this.initToolTip();
    }

    modalDeleteSharedProduct(id){
        let product;
        if(this.sharedFormMode === 'edit') {
            product = this.editSharedList.list.products.find(method => method.id === id);
        }else{
            product = this.newSharedList.list.products.find(method => method.id === id);
        }

        if(product !== null && product !== undefined){
            let checked;
            if(product.checked){
                checked = `<input type="checkbox" checked="checked" disabled="disabled" class="filled-in"/>`;
            }else{
                checked = `<input type="checkbox"  disabled="disabled" class="filled-in"/>`;
            }
            let html =
                `<tr>` +
                '<td>' +
                '<p>' +
                '<label>' +
                checked +
                '<span></span>' +
                '</label>' +
                '</p>' +
                '</td>' +
                '<td>' + product.label + '</td> ' +
                '<td> <div class="round-quantity">' + product.quantity + ' </div></td> ' +
                '</tr>';
            $('#table_delete_shared_product').innerHTML = html;
            this.getModal("#deleteSharedProductModal").open();
            this.resizeModal('#deleteSharedProductModal');
            $('#deleteSharedProductModalTitle').innerHTML = 'Etes-vous sûr de vouloir supprimer le produit: "' + product.label + ' "';
            $('#confirmDeleteSharedProductButton').setAttribute('data-id', id);
        }else{
            this.toast(this.constants.ERROR_DEFAULT, 'danger');
        }

    }

    async deleteSharedProduct(){
        const id = $('#confirmDeleteSharedProductButton').getAttribute('data-id');
        let product;
        if(this.sharedFormMode === 'edit') {
            product = this.editSharedList.list.products.find(method => method.id === parseInt(id))
        }else{
            product = this.newSharedList.list.products.find(method => method.id === parseInt(id))
        }
        if(product !== null && product !== undefined){
            if(this.sharedFormMode === 'edit') {
                await this.model.deleteProduct(id).then(res => {
                    if(res !== 200 && res !== 304){
                        this.displayServiceError(res);
                    }else{
                        this.editSharedList.list.products = this.editSharedList.list.products.filter(function(item) {
                            return item.id != id;
                        });
                        this.refreshAddSharedProduct();
                        this.toast('Produit "'+ product.label + '" supprimé' , 'success');
                    }
                });
            }else{
                this.newSharedList.list.products = this.newSharedList.list.products.filter(function(item) {
                    return item.id != id;
                });
                this.refreshAddSharedProduct();
                this.toast('Produit "'+ product.label + '" supprimé' , 'success');
            }
        }else{
            this.toast(this.constants.ERROR_DEFAULT, 'danger');
        }
        this.getModal("#deleteSharedProductModal").close();
    }

    setValuesModalEditSharedList(idSharedList, idList){
        this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('data-mode', 'edit');
        this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('data-id', idList);

        this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.setAttribute('data-mode', 'edit');
        this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.setAttribute('data-id', idSharedList);

        this.model.getSharedList(idSharedList).then(sharedList => {

            if(sharedList){
                let listName = sharedList.list.shop ? sharedList.list.shop : 'Erreur';
                let listDate = sharedList.list.date ? sharedList.list.date.toLocaleDateString() : 'Erreur date';
                this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.value = listName;
                this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.value = listDate;
                this.editSharedList.list = sharedList.list;
                this.editSharedList.users = sharedList.users;
                this.editSharedList.owner = sharedList.owner;
                this.refreshAddSharedProduct();
                this.refreshAddUser();

                if($('#sharedListNameLabel').className !== 'active'){
                    $('#sharedListNameLabel').className += "active";
                }
                if($('#sharedListQuantityLabel').className !== 'active'){
                    $('#sharedListQuantityLabel').className += "active";
                }
                let htmlTitle = '<div class="input-field col s12"> <i class="material-icons prefix ">shopping_cart</i> <a class="title-header-modal" href="#" >Edition de la liste partagée : ' + listName + '</a> </div>';
                $('#titleHeaderAddSharedList').innerHTML = htmlTitle;

                if(this.permission === 'read'){
                    this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
                    this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.setAttribute('disabled','disabled');
                    this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
                    this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.setAttribute('disabled','disabled');
                    this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
                    this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
                    this.constants.SHARED_USER_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
                    this.constants.SHARED_USER_PERMISSION_INPUT_SELECTOR.setAttribute('disabled','disabled');
                    document.getElementById('formsharedUser').style = 'display: none;'
                }else{
                    this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.removeAttribute('disabled','disabled');
                    this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.removeAttribute('disabled','disabled');
                    this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.removeAttribute('disabled','disabled');
                    this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.removeAttribute('disabled','disabled');
                    this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.removeAttribute('disabled','disabled');
                    this.constants.SHARED_USER_NAME_INPUT_SELECTOR.removeAttribute('disabled','disabled');
                    this.constants.SHARED_USER_PERMISSION_INPUT_SELECTOR.removeAttribute('disabled','disabled');
                    document.getElementById('formsharedUser').style = 'display: block;'
                }

                if(this.editSharedList.owner.id !== this.user.id){
                    document.getElementById('formsharedUser').style = 'display: none;'
                }else{
                    document.getElementById('formsharedUser').style = 'display: block;'
                }

            }else{
                let textError = this.constants.HTTP_ERRORS_DEFAULT;
                this.toast(textError, 'danger');
            }
        });

    }

    modalDeleteSharedList(index, listName){

        let sharedList =  this.sharedLists.filter(function(item) {
            return item.id == index;
        });

        if(sharedList !== null && sharedList !== undefined && sharedList[0] !== null && sharedList[0] !== undefined){
            sharedList = sharedList[0];
            let checked;
            if(sharedList.list.checked){
                checked = `<input type="checkbox" checked="${checked}" disabled="disabled" class="filled-in"/>`;
            }else{
                checked = `<input type="checkbox"  disabled="disabled" class="filled-in"/>`;
            }
            let date = sharedList.list.date ? sharedList.list.date.toLocaleDateString() : 'Erreur date';
            let productsHtml = this.generateLiProductHtml(sharedList.list.products);
            $('#table_delete_shared_list').innerHTML = `<tr>
                        <td>
                            <p>
                              <label>
                                ${checked}
                                <span></span>
                              </label>
                            </p>
                        </td>
                        <td>${sharedList.list.shop}</td>
                        <td>${date}</td>
                        <td>${productsHtml}</td>`;
            this.getModal("#deleteSharedListModal").open();
            this.resizeModal('#deleteSharedListModal');
            $('#deleteSharedListModalTitle').innerHTML = 'Etes-vous sûr de vouloir supprimer la liste partagée: ' + listName;
            $('#confirmDeleteSharedListButton').setAttribute('data-id', sharedList.id);
        }else {
            let textError = this.constants.HTTP_ERRORS_DEFAULT;
            this.toast(textError, 'danger');
        }
    }

    async deleteSharedList(){
        const index = $('#confirmDeleteSharedListButton').getAttribute('data-id');
        let sharedList =  this.sharedLists.filter(function(item) {
            return item.id == index;
        });

        if(sharedList !== null && sharedList !== undefined && sharedList[0] !== null && sharedList[0] !== undefined){
            sharedList = sharedList[0];
            this.lastDeletedSharedLists.push(sharedList);
            await this.model.deleteSharedList(sharedList).then(res => {
                if(res !== 200 && res !== 304){
                    this.displayServiceError(res);
                }else{
                    this.sharedLists.splice(index, 1);
                    this.displayAllLists();
                    this.toast(`<span>Liste  "${sharedList.list.shop}"  supprimée  </span><a class="waves-effect waves-light btn btn-toast ml-20px" onclick="indexController.rollBackDeleteSharedList(${sharedList.id});M.Toast.getInstance(this.parentElement).dismiss();"><i class="material-icons left">settings_backup_restore</i><strong>RESTAURER</strong></a>`, `success`,15000, true);
                }
            });
        }else {
            let textError = this.constants.HTTP_ERRORS_DEFAULT;
            this.toast(textError, 'danger');
        }
        await this.displayAllLists();
        this.getModal("#deleteSharedListModal").close();
    }

    async rollBackDeleteSharedList(index){
        let sharedList =  this.lastDeletedSharedLists.filter(function(item) {
            return item.id == index;
        })
        if(sharedList !== null && sharedList !== undefined && sharedList[0] !== null && sharedList[0] !== undefined){
            sharedList = sharedList[0];
            sharedList.list.date = sharedList.list.date.toLocaleDateString();
            sharedList.owner = sharedList.owner.id;
            await sharedList.users.forEach( user => {
                sharedList.user_id = user.id;
                this.model.insertSharedList(sharedList).then(res => {
                    if(res !== 200 && res !== 304){
                        this.displayServiceError(res);
                    }else{
                        this.displayAllLists();
                        this.toast('Liste partagée ' + sharedList.list.shop + ' ajoutée', 'success');
                    }
                });
            })
        }else {
            let textError = this.constants.HTTP_ERRORS_DEFAULT;
            this.toast(textError, 'danger');
        }
        await this.displayAllLists();
    }

    async deleteUser(){
        const userId = $('#confirmDeleteUserButton').getAttribute('data-id');
        let user;
        if(this.sharedFormMode === 'edit') {
            user = this.editSharedList.users.find(method => method.id === parseInt(userId))
        }else{
            user = this.newSharedList.users.find(method => method.id === parseInt(userId))
        }
        if(user !== null && user !== undefined){
            if(this.sharedFormMode === 'edit') {
                await this.model.deleteUserFromSharedList(this.editSharedList.list.id, userId).then(res => {
                    if(res !== 200 && res !== 304){
                        this.displayServiceError(res);
                    }else{
                        this.editSharedList.users = this.editSharedList.users.filter(function(item) {
                            return item.id != userId;
                        });
                        this.refreshAddUser();
                        this.toast('Utilisateur "'+ user.username + '" supprimé' , 'success');
                    }
                });
            }else{
                this.editSharedList.users =this.editSharedList.users.filter(function(item) {
                    return item.id != userId;
                });
                this.refreshAddUser();
                this.toast('Utilisateur "'+ user.username + '" supprimé' , 'success');
            }
        }else{
            this.toast(this.constants.ERROR_DEFAULT, 'danger');
        }
        this.getModal("#deleteUserModal").close();
    }

    async modalDeleteUser(id){
        let user;

        if(this.sharedFormMode === 'edit') {
            user = this.editSharedList.users.find(method => method.id === parseInt(id))
        }else{
            user = this.newSharedList.users.find(method => method.id === parseInt(id))
        }
        if(user){
            let select = "";
            if(user.permission === "write"){
                select = `<div class="input-field col s2">
                                    <select id="user-${user.id}"  disabled>
                                        <option value="read">Lecture</option>
                                        <option value="write" selected>Ecriture</option>
                                   </select>
                               </div>`;
            }else{
                select = `<div class="input-field col s2">
                                    <select id="user-${user.id}" disabled >
                                        <option value="read" selected>Lecture</option>
                                        <option value="write" >Ecriture</option>
                                   </select>
                               </div>`;
            }
            $('#table_delete_user').innerHTML = '<tr>' +
                '<td>' + user.username + '</td>' +
                '<td>' + user.email + '</td>' +
                '<td>' +
                select +
                '</td>' +
                '</tr>';
            this.getModal("#deleteUserModal").open();
            this.resizeModal('#deleteUserModal');
            $('#deleteUserModalTitle').innerHTML = 'Etes-vous sûr de vouloir supprimer l\'utilisateur: "' + user.username + ' "';
            $('#confirmDeleteUserButton').setAttribute('data-id', id);
            this.initSelect();
        }else{
            this.toast(this.constants.ERROR_DEFAULT, 'danger');
        }
    }

    refreshAddUser(){
        let html = '';
        let users;
        let owner;
        let selected = Array();
        let select = "";

        if(this.sharedFormMode === 'edit') {
            if(this.editSharedList.owner){
                owner = this.editSharedList.owner;
            }
            users = this.editSharedList.users;
        }else{
            owner = this.user;
            users = this.newSharedList.users;
        }
        html +=
            '<tr>' +
                '<td>' + owner.username + '</td>' +
                '<td>' + owner.email + '</td>' +
                '<td class="custom-color"><div class="input-field col s12 creator-title-select"> Créateur </div>' +
                    `<div class="input-field col s4 margin-top-0">
                        <select >
                            <option value="write" >Ecriture</option>
                       </select>
                   </div>`+
                '</td>' +
                '<td></td>' +
            '</tr>';
        let index = 0;

        users.forEach(user => {

            if(this.sharedFormMode === 'add') {
                user.id = index;
            }

            if(user.permission === "write"){
                selected.push('write');
                select = `<div class="input-field col s4">
                            <select id="user-${user.id}" class="user-permission">
                                <option value="write" >Ecriture</option>
                           </select>
                        </div>`;
            }else{
                selected.push('read');
                select = `<div class="input-field col s4">
                            <select id="user-${user.id}" class="user-permission" >
                                <option value="read" >Lecture</option>
                           </select>
                        </div>`;
            }
            html +=
                '<tr>' +
                    '<td>' + user.username + '</td>' +
                    '<td>' + user.email + '</td>' +
                    '<td>' + select +'</td>';
                if(this.user.id === owner.id){
                    html +=
                    '<td>' +
                        '<i onclick="indexController.modalDeleteUser('+ user.id +')" ' +
                        'class="material-icons orange-text pointer tooltipped" data-position="right" data-tooltip="Supprimer">' +
                        'delete' +
                        '</i>' +
                    '</td>';
                }else{
                    html +=
                    '<td></td>';
                }
            html +=
                '</tr>';
            index++;
        });
        this.constants.SHARED_LIST_USER_TBODY_CONTAINER_SELECTOR.innerHTML = html;

        const selectSelector = document.getElementsByClassName('user-permission');
        for (let i = 0; i < selectSelector.length; i++) {
            this.$j = jQuery.noConflict();
            this.$j(selectSelector[i]).val(selected[i]);
        }
        this.initToolTip();
        this.initSelect();
    }

    async addUser(){
        let userNameInput =  this.constants.SHARED_USER_NAME_INPUT_SELECTOR.value;
        let userPermissionInput =  this.constants.SHARED_USER_PERMISSION_INPUT_SELECTOR.value;
        let mode = this.sharedFormMode;

        /* Vérification utilisateur input */
        if(userNameInput === '' || userNameInput === null || userNameInput === undefined){
            this.constants.SHARED_USER_NAME_INPUT_SELECTOR.className = 'validate invalid';
            let containerSearchUser = document.getElementsByClassName("container-search-user");
            containerSearchUser[0].style = "height: 0px;max-height: 0px;"
            this.toast('Champ "Nom du produit" requis', 'warn');
            return;
        }

        /* Vérifie si l'utilisateur existe */
        const users = await this.model.searchUserByEmailAndUsername(userNameInput).catch(e => { this.displayServiceError(); return;});
        if(!users[0]){
            this.constants.SHARED_USER_NAME_INPUT_SELECTOR.className = 'validate invalid';
            let containerSearchUser = document.getElementsByClassName("container-search-user");
            containerSearchUser[0].style = "height: 0px;max-height: 0px;"
            this.toast("Cet utilisateur n'existe pas", 'warn');
            return;
        }

        /* Vérifie si l'utilisateur est le créateur de la liste */
        if(users[0].id === this.user.id){
            this.constants.SHARED_USER_NAME_INPUT_SELECTOR.className = 'validate invalid';
            let containerSearchUser = document.getElementsByClassName("container-search-user");
            containerSearchUser[0].style = "height: 0px;max-height: 0px;"
            this.toast("Vous êtes déjà le créateur de la liste partagée", 'warn');
            return;
        }

        let userAlreadyExist = false;
        if(mode === "edit"){
            this.editSharedList.users.forEach(user => {
                if(users[0].id ===  user.id){
                    userAlreadyExist = true;
                }
            });
        }else{
            this.newSharedList.users.forEach(user => {
                if(users[0].id ===  user.id){
                    userAlreadyExist = true;
                }
            });
        }

        /* Vérifie si l'utilisateur est déja associé à la liste */
        if(userAlreadyExist){
            this.constants.SHARED_USER_NAME_INPUT_SELECTOR.className = 'validate invalid';
            let containerSearchUser = document.getElementsByClassName("container-search-user");
            containerSearchUser[0].style = "height: 0px;max-height: 0px;"
            this.toast("Cet utilisateur est déjà dans la liste partagée", 'warn');
            return;
        }

        /* Vérifie la permission */
        if(userPermissionInput === '' || userPermissionInput === null || userPermissionInput === undefined){
            this.constants.SHARED_USER_PERMISSION_INPUT_SELECTOR.className = 'validate invalid';
            let containerSearchUser = document.getElementsByClassName("container-search-user");
            containerSearchUser[0].style = "height: 0px;max-height: 0px;"
            this.toast('Champ "Permission" requis', 'warn');
            return;
        }

        if(mode === "edit"){
            this.editSharedList.permission = userPermissionInput;
            users[0].permission = userPermissionInput;
            this.editSharedList.owner = this.user.id;
            this.editSharedList.users.push(users[0]);
            this.editSharedList.user_id = users[0].id;

            if(this.editSharedList.list.id !== null){
                this.editSharedList.list.id = parseInt(this.editSharedList.list.id);

                this.model.insertSharedList(this.editSharedList).then(sharedList => {
                    if(sharedList){
                        this.refreshAddUser();
                        this.toast('Utilisateur "' + userNameInput + '" ajouté', 'success');
                        this.constants.SHARED_USER_NAME_INPUT_SELECTOR.value = "";
                        this.constants.SHARED_USER_PERMISSION_INPUT_SELECTOR.value = "";
                    }
                })
            }else{
                this.displayServiceError();
            }

        }else{
            this.newSharedList.permission = userPermissionInput;
            users[0].permission = userPermissionInput;
            this.newSharedList.owner = this.user.id;
            this.newSharedList.users.push(users[0]);
            this.newSharedList.user_id = users[0].id;

            if(this.newSharedList.list.id === undefined || this.newSharedList.list.id === null  ){
                await this.model.insertListReturningId(this.newSharedList.list).then(list => {
                    if(list.id !== null){
                        this.newSharedList.list.id =  parseInt(list.id);
                        this.model.insertSharedList(this.newSharedList).then(sharedList => {
                            if(sharedList){
                                this.refreshAddUser();
                                this.toast('Utilisateur "' + userNameInput + '" ajouté', 'success');
                                this.constants.SHARED_USER_NAME_INPUT_SELECTOR.value = "";
                                this.constants.SHARED_USER_PERMISSION_INPUT_SELECTOR.value = "";
                            }
                        })
                    }else{
                        this.displayServiceError();
                    }
                })
            }else{
                this.model.insertSharedList(this.newSharedList).then(sharedList => {
                    if(sharedList){
                        this.refreshAddUser();
                        this.toast('Utilisateur "' + userNameInput + '" ajouté', 'success');
                        this.constants.SHARED_USER_NAME_INPUT_SELECTOR.value = "";
                        this.constants.SHARED_USER_PERMISSION_INPUT_SELECTOR.value = "";
                    }else{
                        this.displayServiceError();
                    }
                })
            }
        }

    }

    async setUserValue(user){
        const users = await this.model.searchUserByEmailAndUsername(user).catch(e => {this.displayServiceError(); });
        if(users[0]){
            this.constants.SHARED_USER_NAME_INPUT_SELECTOR.value = users[0].email;
            let containerSearchUser = document.getElementsByClassName("container-search-user");
            containerSearchUser[0].style = "height: 100px;max-height: 0px;";
        }
    }

    async searchUser(){

        let containerSearchUser = document.getElementsByClassName("container-search-user");
        let userSearchResult = document.getElementById("user-search-result");

        let userInput = document.getElementById("sharedUserNameInput");
        if(userInput.value === "" || userInput.value === null ){
            containerSearchUser[0].style = "height: 0px;max-height: 0px;"
        }else{

            containerSearchUser[0].style = "height: 150px;max-height: 150px;"
            const users = await this.model.searchUserByEmailAndUsername(userInput.value).catch(e => {
            });

            let html = "";
            if(users && users.length > 0){
                users.forEach(user => {
                    if(user.id !== this.user.id){
                        html += `<li class='li-search-user flex'>
                            <a class="waves-effect flex custom-color pl-15px" onclick="indexController.setUserValue('${user.email}')" href="#"><i class="material-icons">account_circle</i>
                                <p>${user.email}</p>
                            </a>
                        </li>`;
                    }

                });
            }else{
                html = `<li class='li-search-user flex'>
                            <a class="waves-effect flex custom-color pl-15px" ><i class="material-icons">priority_high</i>
                                <p>Aucun utilisateur trouvé</p>
                            </a>
                        </li>`;
            }
            userSearchResult.innerHTML = html;
        }

    }

    async switchCheckedStateSharedProduct(index){
        if(this.sharedFormMode === 'edit') {
            let product =  this.editSharedList.list.products.filter(function(item) {
                return item.id === index;
            });
            if(product !== null && product !== undefined && product[0] !== null && product[0] !== undefined){
                product = product[0];
                product.checked = !product.checked ;
                await this.model.updateProduct(product).then(res => {
                    if(res !== 200 && res !== 304){
                        this.displayServiceError(res);
                    }else{
                        this.displayAllLists();
                        this.toast('Produit "' + product.label + '"  éditée ', 'success');
                    }
                });
            }else {
                let textError = this.constants.HTTP_ERRORS_DEFAULT;
                this.toast(textError, 'danger');
            }
        }else{
            let product =  this.newSharedList.list.products.filter(function(item) {
                return item.id === index;
            });
            if(product !== null && product !== undefined && product[0] !== null && product[0] !== undefined){
                product = product[0];
                product.checked = !product.checked ;
                this.toast('Produit "' + product.label + '"  éditée ', 'success');
            }else {
                let textError = this.constants.HTTP_ERRORS_DEFAULT;
                this.toast(textError, 'danger');
            }
        }
        this.refreshAddSharedProduct();

    }

    hideStep2Shared(){
        setTimeout(function(){
            let elem = document.getElementById("step-content-shred-product");
            elem.style = "display: none";
        }, 100);
    }


    /* Fonctions Listes Classiques */

    displayAllPersonnalLists(){
        const currentDate = new Date();
        currentDate.setHours(0,0,0,0);
        return new Promise((resolve, reject) =>
            this.model.getAllLists(this.user.id).then(lists => {
                let html = "";
                this.lists = lists;
                if(lists.length === 0){
                    html = "<h6 class='text-empty-list'>Aucunes Listes</h6>";
                    document.getElementById('addPulseButton').className = "btn-floating custom-fab-button btn-large custom-bg-color2 pulse tooltipped";
                }else{
                    document.getElementById('addPulseButton').className = "btn-floating custom-fab-button btn-large custom-bg-color2 tooltipped";
                    for(let list of lists) {
                        if( list.date < currentDate && !list.archived){
                            this.setListArchived(list, true);
                        }
                        if(!list.archived){
                            let checked;
                            if(list.checked){
                                checked = `<input onclick="indexController.switchCheckedStateList(${list.id})" type="checkbox" checked="${checked}" class="filled-in"/>`;
                            }else{
                                checked = `<input onclick="indexController.switchCheckedStateList(${list.id})" type="checkbox" class="filled-in"/>`;
                            }
                            let date = list.date ? list.date : 'Erreur date';
                            let warning = '';
                            if(list.date === currentDate && list.checked === false){
                                warning += ` <i class="material-icons orange-text tooltipped warning-list-icon " data-position="top" data-tooltip="Attention: Date de fin aujourd'hui">warning</i>`;
                            }
                            let productsHtml = this.generateLiProductHtml(list.products);
                            html += `<tr id="list${list.id}">
                                        <td class="padding-left-20px">
                                            <p>
                                              <label>
                                              ${warning}
                                                ${checked}
                                                <span></span>
                                              </label>
                                            </p>
                                        </td>
                                        <td>${list.shop}</td>
                                        <td>${date.toLocaleDateString()}</td>
                                        <td >${productsHtml}</td>
                                        <td class="text-align-center">
                                            <i onclick="indexController.modalList('edit');indexController.setValuesModalEditList('${list.id}')" 
                                                class="material-icons custom-color pointer tooltipped icon-table" data-position="top" data-tooltip="Editer">
                                                edit
                                            </i>
                                            <i onclick="indexController.modalDeleteList('${list.id}', '${list.shop}')" 
                                                class="material-icons orange-text pointer tooltipped icon-table" data-position="top" data-tooltip="Supprimer">
                                                delete
                                            </i>
                                         </td>`;
                        }
                    }
                }
                if(this.datatablePerso) {
                    this.destroyDataTablePerso();
                }
                $('#list_container').innerHTML = html;
                this.initDataTablePerso();

                resolve(lists);
            }).catch(e => {this.displayServiceError(e), this.constants.LOADER_SELECTOR.style = "display: none!important;";})
        );
    }

    modalList(mode) {
        this.formMode = mode;
        this.resetStepperFromOtherPage();
        this.formatDatePicker();
        this.resetFormValues();
        this.getModal("#addListModal").open();
        document.getElementById('stepContent1').style.height = 'unset';
        $('#firstStepLi').className = "step active";
        $('#secondStepLi').className = "step";

    }

    checkInputStep1(){
        const listNameInput =  this.constants.LIST_NAME_INPUT_SELECTOR.value;
        const listDateInput =  this.constants.LIST_DATE_INPUT_SELECTOR.value;
        if(listNameInput === '' || listNameInput === null || listNameInput === undefined){
            this.constants.PRODUCT_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.PRODUCT_QUANTITY_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.LIST_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.LIST_NAME_INPUT_SELECTOR.className = 'validate invalid';
            this.toast('Champ "Nom de la liste" requis', 'warn');
            return;
        }
        if(listDateInput === '' || listDateInput === null || listDateInput === undefined){
            this.constants.PRODUCT_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.PRODUCT_QUANTITY_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.LIST_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.LIST_DATE_INPUT_SELECTOR.className = 'validate invalid';
            this.toast('Champ "Date" requis', 'warn');
            return;
        }
        if(this.parseDate(listDateInput) == null){
            this.constants.PRODUCT_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.PRODUCT_QUANTITY_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.constants.LIST_ADD_BUTTON_SELECTOR.setAttribute('disabled','disabled');
            this.toast('Champ "Date" invalide', 'warn');
            document.getElementById('listBackButton').className = 'btn-floating btn btn-flat previous-step custom-bg-color2';
            this.constants.LIST_DATE_INPUT_SELECTOR.className = 'validate invalid';
            return;
        }
        document.getElementById('listBackButton').className = 'btn-floating btn btn-flat previous-step grey lighten-2';
        this.constants.LIST_NAME_INPUT_SELECTOR.className = 'validate valid';
        this.constants.LIST_DATE_INPUT_SELECTOR.className = 'validate valid';
        this.constants.PRODUCT_NAME_INPUT_SELECTOR.removeAttribute('disabled');
        this.constants.PRODUCT_QUANTITY_INPUT_SELECTOR.removeAttribute('disabled');
        this.constants.PRODUCT_ADD_BUTTON_SELECTOR.removeAttribute('disabled');
        this.constants.LIST_ADD_BUTTON_SELECTOR.removeAttribute('disabled');


        let date = this.parseDate(listDateInput);
        if(this.formMode === 'edit') {
            this.editList.shop = listNameInput;
            this.editList.date = date;
            this.editList.user_id = this.user.id;
            this.editList.shared = false;
        }else {
            this.newList.shop = listNameInput;
            this.newList.date = date.toLocaleString();
            this.newList.user_id = this.user.id;
            this.newList.shared = false;
        }

    }

    checkAllProducts(products){
        products.forEach(product => {
            product.checked = true;
            this.model.updateProduct(product).then(res => {
                if(res !== 200 && res !== 304){
                    this.displayServiceError(res);
                }else{
                    this.displayAllLists();
                    this.toast('Produit "' + product.label + '"  éditée ', 'success');
                }
            });
        });
    }

    async switchCheckedStateList(index){
        let list =  this.lists.filter(function(item) {
            return item.id === index;
        });
        if(list !== null && list !== undefined && list[0] !== null && list[0] !== undefined){
            list = list[0];
            list.checked = !list.checked ;
            list.date = list.date.toLocaleDateString();

            if(list.checked){
                this.checkAllProducts(list.products);
            }
            await this.model.updateList(list).then(res => {
                if(res !== 200 && res !== 304){
                    this.displayServiceError(res);
                }else{
                    this.displayAllLists();
                    this.toast('Liste "' + list.shop + '"  éditée ', 'success');
                }
            });
        }else {
            let textError = this.constants.HTTP_ERRORS_DEFAULT;
            this.toast(textError, 'danger');
        }
        await this.displayAllLists();
    }

    async submitAddList() {
        const listName = this.constants.LIST_NAME_INPUT_SELECTOR.value;
        if(this.formMode === 'edit'){
            this.editList.shared = false;
            await this.model.updateList(this.editList).then(res => {
                if(res !== 200 && res !== 304){
                    this.displayServiceError(res);
                }else{
                    this.displayAllLists();
                    this.toast('Liste ' + listName + ' éditée', 'success');
                }
            });
        } else {
            this.newList.archived = false;
            this.newList.shared = false;
            await this.model.insertList(this.newList).then(res => {
                if(res !== 200 && res !== 304){
                    this.displayServiceError(res);
                }else{
                    this.displayAllLists();
                    this.toast('Liste ' + listName + ' ajoutée', 'success');
                }
            });

        }
        this.getModal("#addListModal").close();
        this.displayAllLists();
        this.resetFormValues();
    }

    async setListArchived(list, state){
        if(list !== null &&  list !== undefined && state !== null &&  state !== undefined){
            list.archived = state;
            list.date.toISOString().substr(0, 10);
        }else{
            this.toast(this.constants.ERROR_DEFAULT, 'danger');
            return;
        }
        await this.model.updateList(list).then(res => {
            if(res !== 200 && res !== 304){
                this.displayServiceError(res);
            }else{
                if (list.checked) {
                    this.toast('Liste "' + list.shop + '"  archivée dans l\'historique', 'success');
                } else {
                    this.toast(`<span>Attention: la liste "${list.shop}" est archivée dans l\'historique (liste non réalisée)</span><a class="waves-effect waves-light btn btn-toast ml-20px" onclick="baseController.rollBackArchivedList(${list.id});M.Toast.getInstance(this.parentElement).dismiss();"><i class="material-icons left">settings_backup_restore</i><strong>RESTAURER</strong></a>`, `warn`,15000000, true);
                }
            }
        });
    }

    setValuesModalEditList(index){
        this.constants.PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('data-mode', 'edit');
        this.constants.PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('data-id', index);

        this.constants.LIST_ADD_BUTTON_SELECTOR.setAttribute('data-mode', 'edit');
        this.constants.LIST_ADD_BUTTON_SELECTOR.setAttribute('data-id', index);

        this.model.getList(index).then(list => {
            if(list){
                let listName = list.shop ? list.shop : 'Erreur';
                let listDate = list.date ? list.date.toLocaleDateString() : 'Erreur date';
                this.constants.LIST_NAME_INPUT_SELECTOR.value = listName;
                this.constants.LIST_DATE_INPUT_SELECTOR.value = listDate;
                this.editList = list;
                this.refreshAddProduct();

                if($('#listNameLabel').className !== 'active'){
                    $('#listNameLabel').className += "active";
                }
                if($('#listQuantityLabel').className !== 'active'){
                    $('#listQuantityLabel').className += "active";
                }
                let htmlTitle = '<div class="input-field col s12"> <i class="material-icons prefix ">shopping_cart</i> <a class="title-header-modal" href="#" >Edition de la liste: ' + listName + '</a> </div>';
                $('#titleHeaderAddList').innerHTML = htmlTitle;
            }else{
                let textError = this.constants.HTTP_ERRORS_DEFAULT;
                this.toast(textError, 'danger');
            }
        });

    }

    modalDeleteList(index, listName){

        let list =  this.lists.filter(function(item) {
            return item.id == index;
        });

        if(list !== null && list !== undefined && list[0] !== null && list[0] !== undefined){
            list = list[0];
            let checked;
            if(list.checked){
                checked = `<input type="checkbox" checked="${checked}" disabled="disabled" class="filled-in"/>`;
            }else{
                checked = `<input type="checkbox"  disabled="disabled" class="filled-in"/>`;
            }
            let date = list.date ? list.date.toLocaleDateString() : 'Erreur date';
            let productsHtml = this.generateLiProductHtml(list.products);
            let html = `<tr>
                        <td>
                            <p>
                              <label>
                                ${checked}
                                <span></span>
                              </label>
                            </p>
                        </td>
                        <td>${list.shop}</td>
                        <td>${date}</td>
                        <td>${productsHtml}</td>`;
            $('#table_delete_list').innerHTML = html;
            this.getModal("#deleteListModal").open();
            this.resizeModal('#deleteListModal');
            $('#deleteListModalTitle').innerHTML = 'Etes-vous sûr de vouloir supprimer la liste: ' + listName;
            $('#confirmDeleteListButton').setAttribute('data-id', list.id);
        }else {
            let textError = this.constants.HTTP_ERRORS_DEFAULT;
            this.toast(textError, 'danger');
        }
    }

    async deleteList(){
        const index = $('#confirmDeleteListButton').getAttribute('data-id');
        let list =  this.lists.filter(function(item) {
            return item.id == index;
        });
        if(list !== null && list !== undefined && list[0] !== null && list[0] !== undefined){
            list = list[0];
            this.lastDeletedLists.push(list);
            await this.model.deleteList(index).then(res => {
                if(res !== 200 && res !== 304){
                    this.displayServiceError(res);
                }else{
                    this.lists.splice(index, 1);
                    this.displayAllLists();
                    this.toast(`<span>Liste "${list.shop}"  supprimée  </span><a class="waves-effect waves-light btn btn-toast ml-20px" onclick="indexController.rollBackDeleteList(${list.id});M.Toast.getInstance(this.parentElement).dismiss();"><i class="material-icons left">settings_backup_restore</i><strong>RESTAURER</strong></a>`, `success`,15000, true);
                }
            });
        }else {
            let textError = this.constants.HTTP_ERRORS_DEFAULT;
            this.toast(textError, 'danger');
        }
        this.displayAllLists();
        this.getModal("#deleteListModal").close();
    }

    async rollBackDeleteList(index){
        let list =  this.lastDeletedLists.filter(function(item) {
            return item.id == index;
        })
        if(list !== null && list !== undefined && list[0] !== null && list[0] !== undefined){
            list = list[0];
            list.date = list.date.toLocaleDateString();
            await this.model.insertList(list).then(res => {
                if(res !== 200 && res !== 304){
                    this.displayServiceError(res);
                }else{
                    this.displayAllLists();
                    this.toast('Liste ' + list.shop + ' ajoutée', 'success');
                }
            });
        }else {
            let textError = this.constants.HTTP_ERRORS_DEFAULT;
            this.toast(textError, 'danger');
        }
        this.displayAllLists();
    }

    async switchCheckedStateProduct(index){
        if(this.formMode === 'edit') {
            let product =  this.editList.products.filter(function(item) {
                return item.id === index;
            });
            if(product !== null && product !== undefined && product[0] !== null && product[0] !== undefined){
                product = product[0];
                product.checked = !product.checked ;
                await this.model.updateProduct(product).then(res => {
                    if(res !== 200 && res !== 304){
                        this.displayServiceError(res);
                    }else{
                        this.displayAllLists();
                        this.toast('Produit "' + product.label + '"  éditée ', 'success');
                    }
                });
            }else {
                let textError = this.constants.HTTP_ERRORS_DEFAULT;
                this.toast(textError, 'danger');
            }
        }else{
            let product =  this.newList.products.filter(function(item) {
                return item.id === index;
            });
            if(product !== null && product !== undefined && product[0] !== null && product[0] !== undefined){
                product = product[0];
                product.checked = !product.checked ;
                this.toast('Produit "' + product.label + '"  éditée ', 'success');
            }else {
                let textError = this.constants.HTTP_ERRORS_DEFAULT;
                this.toast(textError, 'danger');
            }
        }
        this.refreshAddProduct();

    }

    async addProduct(){
        let productNameInput =  this.constants.PRODUCT_NAME_INPUT_SELECTOR.value;
        let productQuantityInput =  this.constants.PRODUCT_QUANTITY_INPUT_SELECTOR.value;

        if(productNameInput === '' || productNameInput === null || productNameInput === undefined){
            this.constants.PRODUCT_NAME_INPUT_SELECTOR.className = 'validate invalid';
            this.toast('Champ "Nom du produit" requis', 'warn');
            return;
        }
        if(productQuantityInput === '' || productQuantityInput === null || productQuantityInput === undefined){
            this.constants.PRODUCT_QUANTITY_INPUT_SELECTOR.className = 'validate invalid';
            this.toast('Champ "Quantité" requis', 'warn');
            return;
        }

        let product = new Product(0, false, productNameInput, productQuantityInput);
        const mode = this.constants.PRODUCT_ADD_BUTTON_SELECTOR.getAttribute('data-mode');
        const indexList = this.constants.PRODUCT_ADD_BUTTON_SELECTOR.getAttribute('data-id');

        if(mode === 'edit'){
            this.editList.products.push(product);
            product.list_id = indexList;
            await this.model.insertProduct(product).then(res => {
                if(res !== 200 && res !== 304){
                    this.displayServiceError(res);
                }else{
                    this.model.getProductsByList(product.list_id).then(products => {
                        this.editList.products = products;
                        this.displayAllLists();
                        this.refreshAddProduct();
                        this.toast('Produit "' + productNameInput + '" ajouté', 'success');
                        this.constants.PRODUCT_NAME_INPUT_SELECTOR.value = "";
                        this.constants.PRODUCT_QUANTITY_INPUT_SELECTOR.value = "";
                    });
                }
            });
        }else{
            this.newList.products.push(product);
            await this.displayAllLists();
            this.refreshAddProduct();
            this.toast('Produit "' + productNameInput + '" ajouté', 'success');
            this.constants.PRODUCT_NAME_INPUT_SELECTOR.value = "";
            this.constants.PRODUCT_QUANTITY_INPUT_SELECTOR.value = "";
        }

    }

    refreshAddProduct(){
        let html = '';
        let products;
        if(this.formMode === 'edit') {
            products = this.editList.products;
        }else{
            products = this.newList.products;
        }
        let index = 0;
        products.forEach(product => {
            if(this.formMode === 'add') {
                product.id = index;
            }
            let checked = false;
            if(product.checked){
                checked = `<input onclick="indexController.switchCheckedStateProduct(${product.id}, ${product.checked})" type="checkbox" checked="${checked}" class="filled-in"/>`;
            }else{
                checked = `<input onclick="indexController.switchCheckedStateProduct(${product.id}, ${product.checked})" type="checkbox" class="filled-in"/>`;
            }
            html +=
                '<tr>' +
                    '<td>' +
                        '<p>' +
                            '<label>' +
                                checked +
                                '<span></span>' +
                            '</label>' +
                        '</p>' +
                    '</td>' +
                    '<td>' + product.label + '</td>' +
                    '<td class="position-relative"> <div class="round-quantity">' + product.quantity + '</div></td>' +
                    '<td class="text-align-center">' +
                    '<i onclick="indexController.modalDeleteProduct('+ product.id +')" ' +
                        'class="material-icons orange-text pointer tooltipped" data-position="right" data-tooltip="Supprimer">' +
                        'delete' +
                    '</i>' +
                    '</td>' +
                '<tr>';
            index++;
        })
        this.constants.LIST_PRODUCT_CONTAINER_SELECTOR.innerHTML = html;
        this.initToolTip();
    }

    modalDeleteProduct(id){
        let product;
        if(this.formMode === 'edit') {
            product = this.editList.products.find(method => method.id === id);
        }else{
            product = this.newList.products.find(method => method.id === id);
        }

        if(product !== null && product !== undefined){
            let checked;
            if(product.checked){
                checked = `<input type="checkbox" checked="checked" disabled="disabled" class="filled-in"/>`;
            }else{
                checked = `<input type="checkbox"  disabled="disabled" class="filled-in"/>`;
            }
            let html =
                `<tr>` +
                '<td>' +
                '<p>' +
                '<label>' +
                checked +
                '<span></span>' +
                '</label>' +
                '</p>' +
                '</td>' +
                '<td>' + product.label + '</td> ' +
                '<td> <div class="round-quantity">' + product.quantity + ' </div></td> ' +
                '</tr>';
            $('#table_delete_product').innerHTML = html;
            this.getModal("#deleteProductModal").open();
            this.resizeModal('#deleteProductModal');
            $('#deleteProductModalTitle').innerHTML = 'Etes-vous sûr de vouloir supprimer le produit: "' + product.label + ' "';
            $('#confirmDeleteProductButton').setAttribute('data-id', id);
        }else{
            this.toast(this.constants.ERROR_DEFAULT, 'danger');
        }

    }

    async deleteProduct(){
        const id = $('#confirmDeleteProductButton').getAttribute('data-id');
        let product;
        if(this.formMode === 'edit') {
            product = this.editList.products.find(method => method.id === parseInt(id))
        }else{
            product = this.newList.products.find(method => method.id === parseInt(id))
        }
        if(product !== null && product !== undefined){
            if(this.formMode === 'edit') {
                await this.model.deleteProduct(id).then(res => {
                    if(res !== 200 && res !== 304){
                        this.displayServiceError(res);
                    }else{
                        this.editList.products = this.editList.products.filter(function(item) {
                            return item.id != id;
                        });
                        this.refreshAddProduct();
                        this.toast('Produit "'+ product.label + '" supprimé' , 'success');
                    }
                });
            }else{
                this.newList.products = this.newList.products.filter(function(item) {
                    return item.id != id;
                });
                this.refreshAddProduct();
                this.toast('Produit "'+ product.label + '" supprimé' , 'success');
            }
        }else{
            this.toast(this.constants.ERROR_DEFAULT, 'danger');
        }
        this.getModal("#deleteProductModal").close();
    }

}



window.indexController = new IndexController()
