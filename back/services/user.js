const bcrypt = require('bcrypt')
const UserDAO = require('../datamodel/userdao')
const User = require('../datamodel/user')

module.exports = class UserService {

    constructor(db) {
        this.dao = new UserDAO(db)
    }

    insert(username, email, password, registrationDate) {
        return this.dao.insert(new User(username, email, this.hashPassword(password), registrationDate))
    }

    async validatePassword(email, password) {
        const user = await this.dao.getByEmail(email.trim());
        console.log(this.comparePassword(password, user.challenge));
        return this.comparePassword(password, user.challenge)
    }

    comparePassword(password, hash) {
        return bcrypt.compareSync(password, hash)
    }

    hashPassword(password) {
        return bcrypt.hashSync(password, 10)  // 10 : cost factor -> + élevé = hash + sûr
    }
}