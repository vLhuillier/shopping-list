class Model{

    constructor() {
        this.listApi = new ListApiService();

        this.sharedListApi = new SharedListApiService();
        this.productApi = new ProductApiService();
        this.userApi = new UserAPIService();
    }

    /* User */
    async checkAuthentication(token) {
        return new Promise((resolve, reject) =>
            this.listApi.checkAuthentication(token).then(user => {
                if(user){
                    resolve(user);
                }
            }).catch(e => reject(e))
        )
    }

    async getUserByEmail(email) {
        return new Promise((resolve, reject) =>
            this.userApi.getUserByEmail(email).then(user => {
                if(user){
                    resolve(user);
                }
            }).catch(e => reject(e))
        )
    }

    async searchUserByEmailAndUsername(query) {
        return new Promise((resolve, reject) =>
            this.userApi.searchUserByEmailAndUsername(query).then(user => {
                if(user){
                    resolve(user);
                }
            }).catch(e => reject(e))
        )
    }



    /* Liste Partagée */
    async insertSharedList(sharedList) {
        return new Promise((resolve, reject) =>
            this.sharedListApi.insert(sharedList).then(res => { resolve(res.status) }).catch(e => reject(e)))
    }

    async updatePermission(sharedList) {
        return new Promise((resolve, reject) =>
            this.sharedListApi.updatePermission(sharedList).then(res => { resolve(res.status) }).catch(e => reject(e)))
    }

    async deleteUserFromSharedList(listId, userId) {
        return new Promise((resolve, reject) =>
            this.sharedListApi.deleteUserFromSharedList(listId, userId).then(res => { resolve(res.status) }).catch(e => reject(e)))
    }

    async deleteSharedList(sharedList) {
        return new Promise((resolve, reject) =>
            this.sharedListApi.delete(sharedList).then(resSharedList => {
                   resolve(resSharedList.status)
            }).catch(e => reject(e)))
    }

    async getSharedList(id) {
        try {
            const sharedList = Object.assign(new SharedList(), await this.sharedListApi.get(id))
            sharedList.list.date = new Date(sharedList.list.date)
            return sharedList
        } catch (e) {
            if (e === 404) return null
            return undefined
        }
    }

    async getAllSharedLists(userId) {
        let sharedListsArray = [];
        return new Promise((resolve, reject) =>
            this.sharedListApi.getAll(userId).then(sharedLists => {
                if(sharedLists){
                    for (let sharedList of sharedLists) {
                        sharedList.list.date = new Date(sharedList.list.date);
                        sharedListsArray.push(Object.assign(new SharedList(), sharedList));
                    }
                    resolve(sharedListsArray);
                }
            }).catch(e => reject(e))
        )
    }

    async getAllSharedListsArchived(userId) {
        let listsArray = [];
        return new Promise((resolve, reject) =>
            this.listApi.getAllListsArchived(userId).then(lists => {
                if(lists){
                    for (let list of lists) {
                        if(list.archived === true){
                            list.date = new Date(list.date);
                            listsArray.push(Object.assign(new List(), list));
                        }
                    }
                    resolve(listsArray);
                }
            }).catch(e => reject(e))
        )
    }


    /* Liste */
    async insertList(list) {
        return new Promise((resolve, reject) =>
            this.listApi.insert(list).then(res => { resolve(res.status) }).catch(e => reject(e)))
    }

    /* Liste avec retour de l'id  */
    async insertListReturningId(list) {
        return new Promise((resolve, reject) =>
            this.listApi.insertWithIdReturned(list).then(res => { resolve(res)}).catch(e => reject(e)))
    }

    async updateList(list) {
        return new Promise((resolve, reject) =>
            this.listApi.update(list).then(res => { resolve(res.status) }).catch(e => reject(e)))
    }

    async deleteList(id) {
        return new Promise((resolve, reject) =>
            this.listApi.delete(id).then(res => { resolve(res.status) }).catch(e => reject(e)))
    }

    async getList(id) {
        try {
            const list = Object.assign(new List(), await this.listApi.get(id))
            list.date = new Date(list.date)
            return list
        } catch (e) {
            if (e === 404) return null
            return undefined
        }
    }

    async getAllLists(userId) {
        let listsArray = [];
        return new Promise((resolve, reject) =>
            this.listApi.getAll(userId).then(lists => {
            if(lists){
                for (let list of lists) {
                    list.date = new Date(list.date);
                    listsArray.push(Object.assign(new List(), list));
                }
                resolve(listsArray);
            }
            }).catch(e => reject(e))
        )
    }

    async getAllListsArchived(userId) {
        let listsArray = [];
        return new Promise((resolve, reject) =>
            this.listApi.getAllListsArchived(userId).then(lists => {
                if(lists){
                    for (let list of lists) {
                        if(list.archived === true){
                            list.date = new Date(list.date);
                            listsArray.push(Object.assign(new List(), list));
                        }
                    }
                    resolve(listsArray);
                }
            }).catch(e => reject(e))
        )
    }


    /* Produit */
    async insertProduct(product) {
        return new Promise((resolve, reject) =>
            this.productApi.insert(product).then(res => { resolve(res.status) }).catch(e => reject(e)))
    }

    async updateProduct(product) {
        return new Promise((resolve, reject) =>
            this.productApi.update(product).then(res => { resolve(res.status) }).catch(e => reject(e)))
    }

    async deleteProduct(id) {
        return new Promise((resolve, reject) =>
            this.productApi.delete(id).then(res => { resolve(res.status) }).catch(e => reject(e)))
    }

    async getProductsByList(idList) {
        let productsArray = [];
        return new Promise((resolve, reject) =>
            this.productApi.getProductsByList(idList).then(products => {
                if(products){
                    for (let product of products) {
                        productsArray.push(Object.assign(new Product(), product));
                    }
                    resolve(productsArray);
                }
            }).catch(e => reject(e))
        )
    }


}