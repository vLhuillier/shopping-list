const productBaseUrl = "http://localhost:3333/product";

class ProductApiService extends BaseAPIService{

    getAll() {
        return fetchJSON(productBaseUrl, this.token)
    }

    get(id) {
        return fetchJSON(`${productBaseUrl}/${id}`, this.token)
    }

    getProductsByList(idList) {
        return fetchJSON(`http://localhost:3333/productByList/${idList}`, this.token)
    }

    delete(id) {
        this.headers.delete('Content-Type')
        return fetch(`${productBaseUrl}/${id}`, { method: 'DELETE', headers: this.headers })
    }

    insert(product) {
        this.headers.set( 'Content-Type', 'application/json' )
        return new Promise((resolve, reject) =>
            fetch(productBaseUrl, {
                method: 'POST',
                headers: this.headers,
                body: JSON.stringify(product)
            }).then(res => {
                resolve(res);
            }).catch(e => reject(e))
        )
    }

    update(product) {
        this.headers.set( 'Content-Type', 'application/json' )
        return fetch(productBaseUrl, {
            method: 'PUT',
            headers: this.headers,
            body: JSON.stringify(product)
        })
    }

}