
class Account extends BaseController {

    constructor() {
        super();
        this.initToolTip();
        this.displayUser();

    }

    async displayUser(){
        this.constants.LOADER_SELECTOR.style = "display: flex!important;";
        await this.checkAuthentication().then( () => {
            if(this.user !== null && this.user !== undefined ){
                document.getElementById('userNameInput').value = this.user.username;
                document.getElementById('emailAccount').value = this.user.email;
                document.getElementById('dateCreatedAccount').value = new Date(this.user.registrationdate).toLocaleDateString();
                this.constants.LOADER_SELECTOR.style = "display: none!important;";
                this.constants.MODAL_LOGIN_SELECTOR.style = "display: none!important;opacity: 0!important;";

            }else{
                this.displayServiceError(401);
                this.constants.LOADER_SELECTOR.style = "display: none!important;";
                this.constants.MODAL_LOGIN_SELECTOR.style = "display: flex!important;opacity: 1!important;";
            }
        }).catch(e => {
            this.displayServiceError(e),
            this.constants.LOADER_SELECTOR.style = "display: none!important;";
            this.constants.MODAL_LOGIN_SELECTOR.style = "display: flex!important;opacity: 1!important;";
        });
    }

    logout(){
        let elems = document.querySelectorAll('.tooltipped');
        elems.close;
        this.initToolTip();
        this.token = null;
        localStorage.setItem("token", '');
        document.location.reload();

    }

}

window.accountController = new Account()
