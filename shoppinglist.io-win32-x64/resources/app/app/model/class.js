
class List {
    constructor(checked, shop, date, archived, user_id, products = Array()) {
        this.checked = checked;
        this.shop = shop;
        this.date = date;
        this.archived = archived;
        this.user_id = user_id;
        this.products = products;
    }
    toString() {
        return `${this.shop} ${this.date}`;
    }
}

class SharedList {
    constructor(list = new List(), owner, users= Array(), permission) {
        this.list = list;
        this.owner = owner;
        this.users = users;
        this.permission = permission;

    }
    toString() {
        return `${this.list.shop} ${this.list.date}`;
    }
}

class Product {
    constructor(id, checked, label, quantity, list_id) {
        this.id = id;
        this.checked = checked;
        this.label = label;
        this.quantity = quantity;
        this.list_id = list_id;
    }
    toString() {
        return `${this.label} x${this.quantity}`;
    }
}

class User {
    constructor(id, username, email, challenge, registrationDate) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.challenge = challenge;
        this.registrationDate = registrationDate;
    }
    toString() {
        return `${this.username}`;
    }
}