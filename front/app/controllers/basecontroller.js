class BaseController {

    constructor(secured) {

        M.AutoInit();
        this.model = new Model()
        if (secured) { this.checkAuthentication() }
        this.constants = new Constants();
        this.stepper = document.querySelector('#stepperList');
        this.stepperInstance = new MStepper(this.stepper, {
            firstActive: 0,
            autoFocusInput: false,
            showFeedbackPreloader: true,
            autoFormCreation: true,
            stepTitleNavigation: true,
            feedbackPreloader: '<div class="spinner-layer spinner-green-only">...</div>'
        });
        this.stepperShared = document.querySelector('#stepperSharedList');
        this.stepperInstanceShared = new MStepper(this.stepperShared, {
            firstActive: 0,
            autoFocusInput: false,
            showFeedbackPreloader: true,
            autoFormCreation: true,
            stepTitleNavigation: true,
            feedbackPreloader: '<div class="spinner-layer spinner-green-only">...</div>'
        });
        this.stepperRestore = document.querySelector('#stepperRestore');
        this.stepperRestoreInstance = new MStepper(this.stepperRestore, {
            firstActive: 0,
            autoFocusInput: false,
            showFeedbackPreloader: true,
            autoFormCreation: true,
            stepTitleNavigation: true,
            feedbackPreloader: '<div class="spinner-layer spinner-green-only">...</div>'
        });
        this.$j = jQuery.noConflict();
        this.initToolTip();
        this.initCookieListDisplay();
        this.initSideNavOnLargeScreen();
        this.user = null;
        this.datatablePerso = null;
        this.datatableShared = null;
        this.datatableHisto = null;
        this.confirmDeleteSharedList = false;
    }

    initSideNavOnLargeScreen(){
        let sidenavFixed = document.getElementById("slide-out-sidenav-fixed");
        if(sidenavFixed){
            if (window.innerWidth > 1050){
                sidenavFixed.style = "display: block";
            }else{
                sidenavFixed.style = "display: none";
            }
        }
    }

    initCookieListDisplay(){
        if(getCookie("swicthIndicator") === "sharedLists"){
            if(document.getElementById("sharedLists") && document.getElementById("personnalLists")){
                this.swicthIndicator('sharedLists');
                document.getElementById("sharedLists").style = "display: block";
                document.getElementById("personnalLists").style = "display: none";
                if(document.getElementById("indicator-sharedLists") && document.getElementById("indicator-personnalLists")){
                    document.getElementById("indicator-sharedLists").className = "active";
                    document.getElementById("indicator-personnalLists").className = "";
                }

            }else{
                if(document.getElementById("sharedLists") && document.getElementById("personnalLists")) {
                    document.getElementById("sharedLists").style = "display: none";
                    document.getElementById("personnalLists").style = "display: block";
                }
                if(document.getElementById("indicator-sharedLists") && document.getElementById("indicator-personnalLists")){
                    document.getElementById("indicator-personnalLists").className = "active";
                    document.getElementById("indicator-sharedLists").className = "";
                }
            }
        }
    }

    async checkAuthentication() {
        if (localStorage.getItem("token") !== null && localStorage.getItem("token") !== '') {
            let token = localStorage.getItem("token");
            await this.model.checkAuthentication(token).then(user =>{
                if(user){
                    this.user = user;
                    let email = '';
                    if(this.user.email.length > 17 ){
                        email = this.user.email.substring(0, 17)  + '...';
                    }else {
                        email = this.user.email;
                    }
                    document.getElementById('emailSideNav').innerHTML = "<i class=\"material-icons account-icon-sidenav\">account_circle</i>" + email;
                    document.getElementById('emailSideNav-sidenav-fixed').innerHTML = "<i class=\"material-icons account-icon-sidenav\">account_circle</i>" + email;
                    return user;
                }
            }).catch(e => {this.displayServiceError(e);});
        }
    }

    resetStepperFromOtherPage(){
        this.stepperInstance = new MStepper(this.stepper, {
            firstActive: 0,
            autoFocusInput: false,
            showFeedbackPreloader: true,
            autoFormCreation: true,
            stepTitleNavigation: true,
            feedbackPreloader: '<div class="spinner-layer spinner-green-only">...</div>'
        });
        this.stepperRestoreInstance = new MStepper(this.stepperRestore, {
            firstActive: 0,
            autoFocusInput: false,
            showFeedbackPreloader: true,
            autoFormCreation: true,
            stepTitleNavigation: true,
            feedbackPreloader: '<div class="spinner-layer spinner-green-only">...</div>'
        });
        this.constants.LIST_NAME_INPUT_SELECTOR.removeAttribute('disabled');
        this.constants.LIST_DATE_INPUT_SELECTOR.removeAttribute('disabled');
        $('#formProduct').style.display = 'block';
        this.constants.LIST_ADD_BUTTON_SELECTOR.style.display = '';
        document.getElementById('listBackButton').className = 'btn-floating btn btn-flat previous-step grey lighten-2';
    }

    checkDateInput(){
        const listDateInput =  this.constants.LIST_DATE_RESTORE_INPUT_SELECTOR.value;
        if(listDateInput === '' || listDateInput === null || listDateInput === undefined){
            $('#restoreListDateInput').className = 'validate invalid';
            this.toast('Champ "Date" requis', 'warn');
            return false;
        }
        if(this.parseDate(listDateInput) == null){
            this.toast('Champ "Date" invalide', 'warn');
            document.getElementById('listBackButton').className = 'btn-floating btn btn-flat previous-step custom-bg-color2';
            $('#restoreListDateInput').className = 'validate invalid';
            return false;
        }
        document.getElementById('listBackButton').className = 'btn-floating btn btn-flat previous-step grey lighten-2';
        //this.LIST_BACK_BUTTON_SELECTOR.className = 'btn-floating btn btn-flat previous-step grey lighten-2';
        this.constants.LIST_NAME_INPUT_SELECTOR.className = 'validate valid';
        this.constants.LIST_DATE_RESTORE_INPUT_SELECTOR.className = 'validate valid';
        this.constants.PRODUCT_NAME_INPUT_SELECTOR.removeAttribute('disabled');
        this.constants.PRODUCT_QUANTITY_INPUT_SELECTOR.removeAttribute('disabled');
        this.constants.PRODUCT_ADD_BUTTON_SELECTOR.removeAttribute('disabled');
        this.constants.LIST_ADD_BUTTON_SELECTOR.removeAttribute('disabled');

        return true;

    }

    modalRestoreList(index, mode){
        let list =  this.listsArchived.filter(function(item) {
            return item.id == index;
        });
        if(list !== null && list !== undefined && list[0] !== null && list[0] !== undefined) {
            list = list[0];
            this.formatDatePicker();
            let checked;
            if(list.checked){
                checked = `<input type="checkbox" checked="${checked}" disabled="disabled"/>`;
            }else{
                checked = `<input type="checkbox"  disabled="disabled"/>`;
            }
            let productsHtmlLi = this.generateLiProductHtml(list.products);
            let date = list.date ? list.date.toLocaleDateString() : 'Erreur date';
            let html = `<tr>
                            <td>
                                <p>
                                  <label>
                                    ${checked}
                                    <span></span>
                                  </label>
                                </p>
                            </td>
                            <td>${list.shop}</td>
                            <td>${date}</td>
                            <td>${productsHtmlLi}</td>`;
            $('#table_restore_list').innerHTML = html;
            this.resizeModal('#restoreListModal');
            this.getModal("#restoreListModal").open();
            $('#restoreListModalTitle').innerHTML = 'Voulez-vous restaurer la liste: ' + list.shop;
            $('#subtitleRestoreList').innerHTML = '<strong>Détail liste: ' + list.shop + '</strong>';
            $('#confirmRestoreButton').setAttribute('data-id', list.id);
            if(mode === 'fromArchivedPage'){
                $('#confirmRestoreButton').setAttribute('data-mode', 'fromArchivedPage');
            }

        }
    }

    async rollBackArchivedList(index, mode ){
        await this.model.getList(index).then(list => {
            if(list !== null && list !== undefined) {
                this.formatDatePicker();
                let checked;
                if (list.checked) {
                    checked = `<input type="checkbox" checked="${checked}" disabled="disabled"/>`;
                } else {
                    checked = `<input type="checkbox"  disabled="disabled"/>`;
                }
                let productsHtmlLi = this.generateLiProductHtml(list.products);
                let date = list.date ? list.date.toLocaleDateString() : 'Erreur date';
                let html = `<tr>
                                <td>
                                    <p>
                                      <label>
                                        ${checked}
                                        <span></span>
                                      </label>
                                    </p>
                                </td>
                                <td>${list.shop}</td>
                                <td>${date}</td>
                                <td>${productsHtmlLi}</td>`;
                $('#table_restore_list').innerHTML = html;
                this.resizeModal('#restoreListModal');
                this.getModal("#restoreListModal").open();
                $('#restoreListModalTitle').innerHTML = 'Voulez-vous restaurer la liste: ' + list.shop;
                $('#subtitleRestoreList').innerHTML = '<strong>Détail liste: ' + list.shop + '</strong>';
                $('#confirmRestoreButton').setAttribute('data-id', list.id);
                if(mode === 'fromArchivedPage'){
                    $('#confirmRestoreButton').setAttribute('data-mode', 'fromArchivedPage');
                }
            }
        }).catch(e => {this.displayServiceError(e), this.constants.LOADER_SELECTOR.style = "display: none!important;";});
    }

    async submitRestoreArchivedList(){
        if(this.checkDateInput()){
            const index = $('#confirmRestoreButton').getAttribute('data-id');
            const mode = $('#confirmRestoreButton').getAttribute('data-mode');
            await this.model.getList(index).then(list => {
                if(list !== null && list !== undefined) {
                    list.archived = false;
                    list.date = this.parseDate(this.constants.LIST_DATE_RESTORE_INPUT_SELECTOR.value);
                    this.model.updateList(list).then(res => {
                        if(res !== 200 && res !== 304){
                            this.displayServiceError(res);
                        }else{
                            if(mode === 'fromArchivedPage'){
                                historyController.displayAllListsArchived();
                            }else{
                                indexController.displayAllLists();
                            }
                            this.getModal("#restoreListModal").close();
                            this.toast('Liste ' + list.shop +' restaurée', 'success');
                        }
                    });
                }else{
                    this.toast('Erreur', 'warn');
                }
                if(mode === 'fromArchivedPage'){
                    historyController.displayAllListsArchived();
                }else{
                    indexController.displayAllLists();
                }
                this.getModal("#restoreListModal").close();
            }).catch(e => { this.constants.LOADER_SELECTOR.style = "display: none!important;";});
        }
    }

    toast(msg, type = 'danger', duration = 5000, action = false) {
        if(type === 'danger'){
            msg = '<i class="material-icons left">error</i>' + msg;
            M.toast({html: msg, classes: 'rounded red', displayLength: duration})
        }else if(type === 'success'){
            msg = '<i class="material-icons left">check</i>' + msg;
            M.toast({html: msg, classes: 'rounded green accent-3', displayLength: duration});
        }else if(type === 'warn'){
            msg = '<i class="material-icons left">warning</i>' + msg;
            M.toast({html: msg, classes: 'rounded orange', displayLength: duration})
        }

        if(action){
            let toast = document.getElementsByClassName('toast')[0];
            if(toast){
                toast.style = "padding: 0;padding-left: 25px;"
            }

        }

    }

    resetFormValues(){
        this.stepperInstance.resetStepper();
        this.stepperInstance.firstActive = 0;
        this.stepperInstance.openStep(0, 0);
        $('#firstStepLi').className = "step active";
        let htmlTitle = '<div class="input-field col s12"> <i class="material-icons prefix ">add_shopping_cart</i> <a class="title-header-modal" href="#" >Nouvelle Liste</a> </div>';
        $('#titleHeaderAddList').innerHTML = htmlTitle;
        this.newList = new List(false, '', new Date('now').toDateString(), Array());

        this.constants.PRODUCT_NAME_INPUT_SELECTOR.value = "";
        this.constants.PRODUCT_QUANTITY_INPUT_SELECTOR.value = "";
        this.constants.LIST_NAME_INPUT_SELECTOR.value = "";
        this.constants.LIST_DATE_INPUT_SELECTOR.value = "";
        this.constants.LIST_NAME_INPUT_SELECTOR.className = "validate";
        this.constants.LIST_DATE_INPUT_SELECTOR.className = "datepicker validate";
        this.constants.LIST_PRODUCT_CONTAINER_SELECTOR.innerHTML = '';

        this.constants.PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('data-mode', 'unset');
        this.constants.PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('data-id', 'unset');

        this.constants.LIST_ADD_BUTTON_SELECTOR.setAttribute('data-mode', 'unset');
        this.constants.LIST_ADD_BUTTON_SELECTOR.setAttribute('data-id', 'unset');

    }

    resetSharedFormValues(){
        this.stepperInstanceShared.resetStepper();
        this.stepperInstanceShared.firstActive = 0;
        this.stepperInstanceShared.openStep(0, 0);
        $('#firstStepLiShared').className = "step active";
        let htmlTitle = '<div class="input-field col s12"> <i class="material-icons prefix ">share</i> <a class="title-header-modal" href="#" >Nouvelle Liste Partagée</a> </div>';
        $('#titleHeaderAddSharedList').innerHTML = htmlTitle;
        this.newList = new List(false, '', new Date('now').toDateString(), Array());

        this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.value = "";
        this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.value = "";
        this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.value = "";
        this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.value = "";
        this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.className = "validate";
        this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.className = "datepicker validate";
        this.constants.SHARED_LIST_PRODUCT_CONTAINER_SELECTOR.innerHTML = '';

        this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('data-mode', 'unset');
        this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('data-id', 'unset');

        this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.setAttribute('data-mode', 'unset');
        this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.setAttribute('data-id', 'unset');

        this.constants.SHARED_LIST_NAME_INPUT_SELECTOR.removeAttribute('disabled','disabled');
        this.constants.SHARED_LIST_DATE_INPUT_SELECTOR.removeAttribute('disabled','disabled');
        this.constants.SHARED_PRODUCT_NAME_INPUT_SELECTOR.removeAttribute('disabled','disabled');
        this.constants.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR.removeAttribute('disabled','disabled');
        this.constants.SHARED_PRODUCT_ADD_BUTTON_SELECTOR.removeAttribute('disabled','disabled');
        this.constants.SHARED_LIST_ADD_BUTTON_SELECTOR.removeAttribute('disabled','disabled');
        this.constants.SHARED_USER_NAME_INPUT_SELECTOR.removeAttribute('disabled','disabled');
        this.constants.SHARED_USER_PERMISSION_INPUT_SELECTOR.removeAttribute('disabled','disabled');
        document.getElementById('formsharedUser').style = 'display: block;'

        this.constants.SHARED_LIST_USER_TBODY_CONTAINER_SELECTOR.innerHTML = '';

    }

    parseDate(date) {
        let m = date.match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/);
        return (m) ? new Date(m[3], m[2]-1, m[1]).toLocaleDateString() : null;
    }

    onConfirmDeleteSharedList(state){
        this.confirmDeleteSharedList = state;
    }

    async closeModal(id){
        let reload = true;
        if(id === "#addSharedListModal"){
            if(this.sharedFormMode === 'edit'){
                if(this.editSharedList.users === null || this.editSharedList.users === undefined || this.editSharedList.users.length === 0){
                    if(!this.confirmDeleteSharedList){
                        this.getModal("#warningModal").open();
                        reload = false;
                    }else{
                        reload = true;
                    }
                }else{
                    reload = true;
                }
            }else{
                if(this.newSharedList.users === null || this.newSharedList.users === undefined || this.newSharedList.users.length === 0){
                    if(!this.confirmDeleteSharedList){
                        this.getModal("#warningModal").open();
                        reload = false;
                    }else{
                        reload = true;
                    }
                }else{
                    reload = true;
                }
            }
        }else{
            reload = true;
        }
        if(reload){
            this.getModal(id).close();
            await indexController.displayAllLists();
        }

    }

    resizeModal(id){
        let height = window.outerHeight;
        let width = window.outerWidth;
        const modal = $(id);
        if(height > width){
            modal.style.minHeight = height * 0.55 + 'px';
            modal.style.minWidth = width * 0.90 + 'px';
        }else{
            modal.style.minHeight = height * 0.70 + 'px';
            modal.style.minWidth = width * 0.85 + 'px';
        }
    }

    generateLiProductHtml(products){
        let productsHtml = '';
        if(products && products.length > 0){
            productsHtml += `<ul>`;
            products.forEach(product => {
                productsHtml += `<li class="flex" style="position: relative; margin-bottom: 5px"> <div class="round-quantity"> ${product.quantity} </div> - ${product.label}</li>`;
            });
            productsHtml += `</ul>`;
        }else{
            productsHtml = 'Aucun produits';
        }
        return productsHtml;
    }

    generateLiUserHtml(users){
        let usersHtml = '';
        if(users && users.length > 0){
            usersHtml += `<ul class="user-ul">`;
            users.forEach(user => {
                usersHtml += `<li class='user-li flex'>
                            <a class=" flex custom-color pl-15px" href="#"><i class="material-icons">account_circle</i>
                                <p>${user.email}</p>
                            </a>
                        </li>`;
            });
            usersHtml += `</ul>`;

        }else{
            usersHtml = 'Aucun Utilisateur';
        }
        return usersHtml;
    }

    initSelect(){
        this.$j = jQuery.noConflict();
        this.$j('select').formSelect();
    }

    initToolTip(){
        document.addEventListener('DOMContentLoaded', function() {
            let elems = document.querySelectorAll('.tooltipped');
        });
    }

    destroyDataTablePerso(){
        this.datatablePerso.clear();
        this.datatablePerso.destroy();
    }

    destroyDataTableShared(){
        this.datatableShared.clear();
        this.datatableShared.destroy();
    }

    destroyDataTableHisto(){
        this.datatableHisto.clear();
        this.datatableHisto.destroy();
    }

    setSizeOfDataTable(){
        let maxHeight = '';
        if(window.innerHeight > 850){
            maxHeight = "400px";
        }else if (window.innerHeight > window.innerWidth){
            maxHeight = "400px";
        }else{
            if(window.innerWidth > 850){
                maxHeight = "250px";
            }else{
                maxHeight = "150px";
            }
        }
        return maxHeight;
    }

    customInitDataTables(){

        setTimeout(function(){
            let sharedListsDatatable = document.getElementById('sharedLists');
            if(sharedListsDatatable){
                let elemToHide =  sharedListsDatatable.children[2].children[2].children[1];
                if(elemToHide.children[0].children[0].children[0]){
                    elemToHide.children[0].children[0].children[0].style = "display: none;";
                }
            }

        }, 100);

        let dataTables_scrollHeadInner = document.getElementsByClassName('dataTables_scrollHeadInner');
        for (let element of dataTables_scrollHeadInner) {
            if(element){
                element.style = "width: 100%;";
            }
        }

        let no_footer_dataTable = document.getElementsByClassName('no-footer dataTable');
        for (let element of no_footer_dataTable) {
            if(element){
                element.style = "width: 100%;";
            }
        }

        setTimeout(function(){
            let searchInputs = document.getElementsByClassName('dataTables_filter');
            let i = 0;
            for (let searchInput of searchInputs) {
                let elementId = document.getElementsByClassName('dataTables_filter')[i];
                searchInput.insertAdjacentHTML("beforeend", `<i onclick='baseController.removeSearch("${elementId.id}")' class="material-icons search-icon" >close</i>`);
                i++;
            }
        }, 100);
    }

    initDataTablePerso(){
        this.$j.fn.dataTable.ext.classes.sPageButton = 'btn-floating btn-small green lighten-4 mr-5px ';
        let maxHeight = this.setSizeOfDataTable();
        let datatablePerso = document.getElementById('datatablePerso');
        this.datatablePerso = this.$j(datatablePerso).DataTable({
            "bDestroy": true,
            responsive: true,
            "scrollY": maxHeight,
            "scrollCollapse": true,
            "stateSave": true,
            "order": [[ 2, "desc" ]],
            "language": {
                "emptyTable": "Aucune donnée disponible dans le tableau",
                "lengthMenu": "Afficher _MENU_ listes",
                "loadingRecords": "Chargement...",
                "processing": "Traitement...",
                "zeroRecords": "Aucun élément correspondant trouvé",
                "paginate": {
                    "first": "Premier",
                    "last": "Dernier",
                    "previous": "<i class=\"large material-icons\">arrow_back</i>",
                    "next": "<i class=\"large material-icons\">arrow_forward</i>"
                },
                "aria": {
                    "sortAscending": ": activer pour trier la colonne par ordre croissant",
                    "sortDescending": ": activer pour trier la colonne par ordre décroissant"
                },
                "select": {
                    "rows": {
                        "_": "%d lignes sélectionnées",
                        "0": "Aucune ligne sélectionnée",
                        "1": "1 ligne sélectionnée"
                    },
                    "1": "1 ligne selectionnée",
                    "_": "%d lignes selectionnées",
                    "cells": {
                        "1": "1 cellule sélectionnée",
                        "_": "%d cellules sélectionnées"
                    },
                    "columns": {
                        "1": "1 colonne sélectionnée",
                        "_": "%d colonnes sélectionnées"
                    }
                },
                "autoFill": {
                    "cancel": "Annuler",
                    "fill": "Remplir toutes les cellules avec <i>%d<\/i>",
                    "fillHorizontal": "Remplir les cellules horizontalement",
                    "fillVertical": "Remplir les cellules verticalement",
                    "info": "Exemple de remplissage automatique"
                },
                "searchBuilder": {
                    "conditions": {
                        "date": {
                            "after": "Après le",
                            "before": "Avant le",
                            "between": "Entre",
                            "empty": "Vide",
                            "equals": "Egal à",
                            "not": "Différent de",
                            "notBetween": "Pas entre",
                            "notEmpty": "Non vide"
                        },
                        "number": {
                            "between": "Entre",
                            "empty": "Vide",
                            "equals": "Egal à",
                            "gt": "Supérieur à",
                            "gte": "Supérieur ou égal à",
                            "lt": "Inférieur à",
                            "lte": "Inférieur ou égal à",
                            "not": "Différent de",
                            "notBetween": "Pas entre",
                            "notEmpty": "Non vide"
                        },
                        "string": {
                            "contains": "Contient",
                            "empty": "Vide",
                            "endsWith": "Se termine par",
                            "equals": "Egal à",
                            "not": "Différent de",
                            "notEmpty": "Non vide",
                            "startsWith": "Commence par"
                        },
                        "array": {
                            "equals": "Egal à",
                            "empty": "Vide",
                            "contains": "Contient",
                            "not": "Différent de",
                            "notEmpty": "Non vide",
                            "without": "Sans"
                        }
                    },
                    "add": "Ajouter une condition",
                    "button": {
                        "0": "Recherche avancée",
                        "_": "Recherche avancée (%d)"
                    },
                    "clearAll": "Effacer tout",
                    "condition": "Condition",
                    "data": "Donnée",
                    "deleteTitle": "Supprimer la règle de filtrage",
                    "logicAnd": "Et",
                    "logicOr": "Ou",
                    "title": {
                        "0": "Recherche avancée",
                        "_": "Recherche avancée (%d)"
                    },
                    "value": "Valeur"
                },
                "searchPanes": {
                    "clearMessage": "Effacer tout",
                    "count": "{total}",
                    "title": "Filtres actifs - %d",
                    "collapse": {
                        "0": "Volet de recherche",
                        "_": "Volet de recherche (%d)"
                    },
                    "countFiltered": "{shown} ({total})",
                    "emptyPanes": "Pas de volet de recherche",
                    "loadMessage": "Chargement du volet de recherche..."
                },
                "buttons": {
                    "copyKeys": "Appuyer sur ctrl ou u2318 + C pour copier les données du tableau dans votre presse-papier.",
                    "collection": "Collection",
                    "colvis": "Visibilité colonnes",
                    "colvisRestore": "Rétablir visibilité",
                    "copy": "Copier",
                    "copySuccess": {
                        "1": "1 ligne copiée dans le presse-papier",
                        "_": "%ds lignes copiées dans le presse-papier"
                    },
                    "copyTitle": "Copier dans le presse-papier",
                    "csv": "CSV",
                    "excel": "Excel",
                    "pageLength": {
                        "-1": "Afficher toutes les lignes",
                        "1": "Afficher 1 ligne",
                        "_": "Afficher %d lignes"
                    },
                    "pdf": "PDF",
                    "print": "Imprimer"
                },
                "decimal": ",",
                "info": "Affichage de _START_ à _END_ sur _TOTAL_ listes",
                "infoEmpty": "Affichage de 0 à 0 sur 0 listes",
                "infoThousands": ".",
                "search": "",
                "searchPlaceholder": "Rechercher",
                "thousands": ".",
                "infoFiltered": "(filtrés depuis un total de _MAX_ listes)",
                "datetime": {
                    "previous": "Précédent",
                    "next": "Suivant",
                    "hours": "Heures",
                    "minutes": "Minutes",
                    "seconds": "Secondes",
                    "unknown": "-",
                    "amPm": [
                        "am",
                        "pm"
                    ]
                },
                "editor": {
                    "close": "Fermer",
                    "create": {
                        "button": "Nouveaux",
                        "title": "Créer une nouvelle entrée",
                        "submit": "Envoyer"
                    },
                    "edit": {
                        "button": "Editer",
                        "title": "Editer Entrée",
                        "submit": "Modifier"
                    },
                    "remove": {
                        "button": "Supprimer",
                        "title": "Supprimer",
                        "submit": "Supprimer"
                    },
                    "error": {
                        "system": "Une erreur système s'est produite"
                    },
                    "multi": {
                        "title": "Valeurs Multiples",
                        "restore": "Rétablir Modification"
                    }
                }
            }
        });

        this.customInitDataTables();
    }

    initDataTableShared(){
        this.$j.fn.dataTable.ext.classes.sPageButton = 'btn-floating btn-small green lighten-4 mr-5px ';
        let maxHeight = this.setSizeOfDataTable();
        let datatableShared = document.getElementById('datatableShared');
        this.datatableShared = this.$j(datatableShared).DataTable({
            "bDestroy": true,
            responsive: true,
            "scrollY": maxHeight,
            "scrollCollapse": true,
            "stateSave": true,
            "order": [[ 2, "desc" ]],
            "language": {
                "emptyTable": "Aucune donnée disponible dans le tableau",
                "lengthMenu": "Afficher _MENU_ listes",
                "loadingRecords": "Chargement...",
                "processing": "Traitement...",
                "zeroRecords": "Aucun élément correspondant trouvé",
                "paginate": {
                    "first": "Premier",
                    "last": "Dernier",
                    "previous": "<i class=\"large material-icons\">arrow_back</i>",
                    "next": "<i class=\"large material-icons\">arrow_forward</i>"
                },
                "aria": {
                    "sortAscending": ": activer pour trier la colonne par ordre croissant",
                    "sortDescending": ": activer pour trier la colonne par ordre décroissant"
                },
                "select": {
                    "rows": {
                        "_": "%d lignes sélectionnées",
                        "0": "Aucune ligne sélectionnée",
                        "1": "1 ligne sélectionnée"
                    },
                    "1": "1 ligne selectionnée",
                    "_": "%d lignes selectionnées",
                    "cells": {
                        "1": "1 cellule sélectionnée",
                        "_": "%d cellules sélectionnées"
                    },
                    "columns": {
                        "1": "1 colonne sélectionnée",
                        "_": "%d colonnes sélectionnées"
                    }
                },
                "autoFill": {
                    "cancel": "Annuler",
                    "fill": "Remplir toutes les cellules avec <i>%d<\/i>",
                    "fillHorizontal": "Remplir les cellules horizontalement",
                    "fillVertical": "Remplir les cellules verticalement",
                    "info": "Exemple de remplissage automatique"
                },
                "searchBuilder": {
                    "conditions": {
                        "date": {
                            "after": "Après le",
                            "before": "Avant le",
                            "between": "Entre",
                            "empty": "Vide",
                            "equals": "Egal à",
                            "not": "Différent de",
                            "notBetween": "Pas entre",
                            "notEmpty": "Non vide"
                        },
                        "number": {
                            "between": "Entre",
                            "empty": "Vide",
                            "equals": "Egal à",
                            "gt": "Supérieur à",
                            "gte": "Supérieur ou égal à",
                            "lt": "Inférieur à",
                            "lte": "Inférieur ou égal à",
                            "not": "Différent de",
                            "notBetween": "Pas entre",
                            "notEmpty": "Non vide"
                        },
                        "string": {
                            "contains": "Contient",
                            "empty": "Vide",
                            "endsWith": "Se termine par",
                            "equals": "Egal à",
                            "not": "Différent de",
                            "notEmpty": "Non vide",
                            "startsWith": "Commence par"
                        },
                        "array": {
                            "equals": "Egal à",
                            "empty": "Vide",
                            "contains": "Contient",
                            "not": "Différent de",
                            "notEmpty": "Non vide",
                            "without": "Sans"
                        }
                    },
                    "add": "Ajouter une condition",
                    "button": {
                        "0": "Recherche avancée",
                        "_": "Recherche avancée (%d)"
                    },
                    "clearAll": "Effacer tout",
                    "condition": "Condition",
                    "data": "Donnée",
                    "deleteTitle": "Supprimer la règle de filtrage",
                    "logicAnd": "Et",
                    "logicOr": "Ou",
                    "title": {
                        "0": "Recherche avancée",
                        "_": "Recherche avancée (%d)"
                    },
                    "value": "Valeur"
                },
                "searchPanes": {
                    "clearMessage": "Effacer tout",
                    "count": "{total}",
                    "title": "Filtres actifs - %d",
                    "collapse": {
                        "0": "Volet de recherche",
                        "_": "Volet de recherche (%d)"
                    },
                    "countFiltered": "{shown} ({total})",
                    "emptyPanes": "Pas de volet de recherche",
                    "loadMessage": "Chargement du volet de recherche..."
                },
                "buttons": {
                    "copyKeys": "Appuyer sur ctrl ou u2318 + C pour copier les données du tableau dans votre presse-papier.",
                    "collection": "Collection",
                    "colvis": "Visibilité colonnes",
                    "colvisRestore": "Rétablir visibilité",
                    "copy": "Copier",
                    "copySuccess": {
                        "1": "1 ligne copiée dans le presse-papier",
                        "_": "%ds lignes copiées dans le presse-papier"
                    },
                    "copyTitle": "Copier dans le presse-papier",
                    "csv": "CSV",
                    "excel": "Excel",
                    "pageLength": {
                        "-1": "Afficher toutes les lignes",
                        "1": "Afficher 1 ligne",
                        "_": "Afficher %d lignes"
                    },
                    "pdf": "PDF",
                    "print": "Imprimer"
                },
                "decimal": ",",
                "info": "Affichage de _START_ à _END_ sur _TOTAL_ listes",
                "infoEmpty": "Affichage de 0 à 0 sur 0 listes",
                "infoThousands": ".",
                "search": "",
                "searchPlaceholder": "Rechercher",
                "thousands": ".",
                "infoFiltered": "(filtrés depuis un total de _MAX_ listes)",
                "datetime": {
                    "previous": "Précédent",
                    "next": "Suivant",
                    "hours": "Heures",
                    "minutes": "Minutes",
                    "seconds": "Secondes",
                    "unknown": "-",
                    "amPm": [
                        "am",
                        "pm"
                    ]
                },
                "editor": {
                    "close": "Fermer",
                    "create": {
                        "button": "Nouveaux",
                        "title": "Créer une nouvelle entrée",
                        "submit": "Envoyer"
                    },
                    "edit": {
                        "button": "Editer",
                        "title": "Editer Entrée",
                        "submit": "Modifier"
                    },
                    "remove": {
                        "button": "Supprimer",
                        "title": "Supprimer",
                        "submit": "Supprimer"
                    },
                    "error": {
                        "system": "Une erreur système s'est produite"
                    },
                    "multi": {
                        "title": "Valeurs Multiples",
                        "restore": "Rétablir Modification"
                    }
                }
            }
        });

        this.customInitDataTables();
    }

    initDataTableHisto(){
        this.$j.fn.dataTable.ext.classes.sPageButton = 'btn-floating btn-small green lighten-4 mr-5px ';
        let maxHeight = this.setSizeOfDataTable();
        let datatableHisto = document.getElementById('datatableHisto');
        this.datatableHisto = this.$j(datatableHisto).DataTable({
            "bDestroy": true,
            responsive: true,
            "scrollY": maxHeight,
            "scrollCollapse": true,
            "stateSave": true,
            "order": [[ 2, "desc" ]],
            "language": {
                "emptyTable": "Aucune donnée disponible dans le tableau",
                "lengthMenu": "Afficher _MENU_ listes",
                "loadingRecords": "Chargement...",
                "processing": "Traitement...",
                "zeroRecords": "Aucun élément correspondant trouvé",
                "paginate": {
                    "first": "Premier",
                    "last": "Dernier",
                    "previous": "<i class=\"large material-icons\">arrow_back</i>",
                    "next": "<i class=\"large material-icons\">arrow_forward</i>"
                },
                "aria": {
                    "sortAscending": ": activer pour trier la colonne par ordre croissant",
                    "sortDescending": ": activer pour trier la colonne par ordre décroissant"
                },
                "select": {
                    "rows": {
                        "_": "%d lignes sélectionnées",
                        "0": "Aucune ligne sélectionnée",
                        "1": "1 ligne sélectionnée"
                    },
                    "1": "1 ligne selectionnée",
                    "_": "%d lignes selectionnées",
                    "cells": {
                        "1": "1 cellule sélectionnée",
                        "_": "%d cellules sélectionnées"
                    },
                    "columns": {
                        "1": "1 colonne sélectionnée",
                        "_": "%d colonnes sélectionnées"
                    }
                },
                "autoFill": {
                    "cancel": "Annuler",
                    "fill": "Remplir toutes les cellules avec <i>%d<\/i>",
                    "fillHorizontal": "Remplir les cellules horizontalement",
                    "fillVertical": "Remplir les cellules verticalement",
                    "info": "Exemple de remplissage automatique"
                },
                "searchBuilder": {
                    "conditions": {
                        "date": {
                            "after": "Après le",
                            "before": "Avant le",
                            "between": "Entre",
                            "empty": "Vide",
                            "equals": "Egal à",
                            "not": "Différent de",
                            "notBetween": "Pas entre",
                            "notEmpty": "Non vide"
                        },
                        "number": {
                            "between": "Entre",
                            "empty": "Vide",
                            "equals": "Egal à",
                            "gt": "Supérieur à",
                            "gte": "Supérieur ou égal à",
                            "lt": "Inférieur à",
                            "lte": "Inférieur ou égal à",
                            "not": "Différent de",
                            "notBetween": "Pas entre",
                            "notEmpty": "Non vide"
                        },
                        "string": {
                            "contains": "Contient",
                            "empty": "Vide",
                            "endsWith": "Se termine par",
                            "equals": "Egal à",
                            "not": "Différent de",
                            "notEmpty": "Non vide",
                            "startsWith": "Commence par"
                        },
                        "array": {
                            "equals": "Egal à",
                            "empty": "Vide",
                            "contains": "Contient",
                            "not": "Différent de",
                            "notEmpty": "Non vide",
                            "without": "Sans"
                        }
                    },
                    "add": "Ajouter une condition",
                    "button": {
                        "0": "Recherche avancée",
                        "_": "Recherche avancée (%d)"
                    },
                    "clearAll": "Effacer tout",
                    "condition": "Condition",
                    "data": "Donnée",
                    "deleteTitle": "Supprimer la règle de filtrage",
                    "logicAnd": "Et",
                    "logicOr": "Ou",
                    "title": {
                        "0": "Recherche avancée",
                        "_": "Recherche avancée (%d)"
                    },
                    "value": "Valeur"
                },
                "searchPanes": {
                    "clearMessage": "Effacer tout",
                    "count": "{total}",
                    "title": "Filtres actifs - %d",
                    "collapse": {
                        "0": "Volet de recherche",
                        "_": "Volet de recherche (%d)"
                    },
                    "countFiltered": "{shown} ({total})",
                    "emptyPanes": "Pas de volet de recherche",
                    "loadMessage": "Chargement du volet de recherche..."
                },
                "buttons": {
                    "copyKeys": "Appuyer sur ctrl ou u2318 + C pour copier les données du tableau dans votre presse-papier.",
                    "collection": "Collection",
                    "colvis": "Visibilité colonnes",
                    "colvisRestore": "Rétablir visibilité",
                    "copy": "Copier",
                    "copySuccess": {
                        "1": "1 ligne copiée dans le presse-papier",
                        "_": "%ds lignes copiées dans le presse-papier"
                    },
                    "copyTitle": "Copier dans le presse-papier",
                    "csv": "CSV",
                    "excel": "Excel",
                    "pageLength": {
                        "-1": "Afficher toutes les lignes",
                        "1": "Afficher 1 ligne",
                        "_": "Afficher %d lignes"
                    },
                    "pdf": "PDF",
                    "print": "Imprimer"
                },
                "decimal": ",",
                "info": "Affichage de _START_ à _END_ sur _TOTAL_ listes",
                "infoEmpty": "Affichage de 0 à 0 sur 0 listes",
                "infoThousands": ".",
                "search": "",
                "searchPlaceholder": "Rechercher",
                "thousands": ".",
                "infoFiltered": "(filtrés depuis un total de _MAX_ listes)",
                "datetime": {
                    "previous": "Précédent",
                    "next": "Suivant",
                    "hours": "Heures",
                    "minutes": "Minutes",
                    "seconds": "Secondes",
                    "unknown": "-",
                    "amPm": [
                        "am",
                        "pm"
                    ]
                },
                "editor": {
                    "close": "Fermer",
                    "create": {
                        "button": "Nouveaux",
                        "title": "Créer une nouvelle entrée",
                        "submit": "Envoyer"
                    },
                    "edit": {
                        "button": "Editer",
                        "title": "Editer Entrée",
                        "submit": "Modifier"
                    },
                    "remove": {
                        "button": "Supprimer",
                        "title": "Supprimer",
                        "submit": "Supprimer"
                    },
                    "error": {
                        "system": "Une erreur système s'est produite"
                    },
                    "multi": {
                        "title": "Valeurs Multiples",
                        "restore": "Rétablir Modification"
                    }
                }
            }
        });

        this.customInitDataTables();
    }

    removeSearch(target){
        if(target === "datatablePerso_filter"){
            this.initDataTablePerso();
            this.datatablePerso.search("").draw();
        }else if (target === "datatableShared_filter"){
            this.initDataTableShared();
            this.datatableShared.search("").draw();
        }else{
            this.initDataTableHisto();
            this.datatableHisto.search("").draw();
        }

    }

    formatDatePicker(){
        this.$j('#listDateInput').datepicker({
            firstDay: true,
            format: 'dd/mm/yyyy',
            minDate: new Date(),
            i18n: {
                months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jui', 'Jui', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
                weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi' ],
                weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"],
                today: 'Aujourd\'hui',
                clear: 'Réinitialiser',
                cancel: 'Fermer'
            }
        });
        this.$j('#sharedListDateInput').datepicker({
            firstDay: true,
            format: 'dd/mm/yyyy',
            minDate: new Date(),
            i18n: {
                months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jui', 'Jui', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
                weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi' ],
                weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"],
                today: 'Aujourd\'hui',
                clear: 'Réinitialiser',
                cancel: 'Fermer'
            }
        });
        this.$j('#restoreListDateInput').datepicker({
            firstDay: true,
            format: 'dd/mm/yyyy',
            minDate: new Date(),
            i18n: {
                months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jui', 'Jui', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
                weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi' ],
                weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"],
                today: 'Aujourd\'hui',
                clear: 'Réinitialiser',
                cancel: 'Fermer'
            }
        });
    }

    displayServiceError(error) {
        let textError = this.constants.HTTP_ERRORS.find(method => method.key === error);
        if(textError !== undefined && textError !== null && textError !== ''){
            textError = textError.text;
        }else{
            textError = this.constants.HTTP_ERRORS_DEFAULT;
        }
        if(error === 401){
            this.toast(textError, 'warn');
        }else{
            this.toast(textError, 'danger');
        }

    }

    getModal(selector) {
        return M.Modal.getInstance($(selector))
    }

    setBackButtonView(view) {
        window.onpopstate = function() {
            navigate(view)
        }; history.pushState({}, '');
    }

    swicthIndicator(target){
        setTimeout(function(){
            let indicator = document.getElementsByClassName('indicator')[0];
            if(target === "personnalLists"){
                setCookie("swicthIndicator", "personnalLists", 365);
                if(indicator) {
                    indicator.style = "left: 0px !important;right: unset !important;width: 50%;";
                }
            }else{
                setCookie("swicthIndicator", "sharedLists", 365);
                if(indicator){
                    indicator.style = "right: 0px !important;left: unset !important;width: 50%;";
                }
            }
        }, 500);
    }

}

window.baseController = new BaseController()