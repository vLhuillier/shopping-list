
class UserAPIService extends BaseAPIService {

    constructor() {
        super("user");
    }

    register(username, email, password) {
        this.headers.set('Content-Type', 'application/x-www-form-urlencoded');
        return new Promise((resolve, reject) => fetch(`${this.url}/register`, {
            method: "POST",
            headers: this.headers,
            body: `username=${username}&email=${email}&password=${password}`
        }).then(res => {
            if (res.status === 200) {
                resolve(res.json());
            } else {
                reject(res.status);
            }
        }).catch(err => reject(err)))
    }


    authenticate(email, password) {
        this.headers.set('Content-Type', 'application/x-www-form-urlencoded');
        return new Promise((resolve, reject) => fetch(`${this.url}/authenticate`, {
            method: "POST",
            headers: this.headers,
            body: `email=${email}&password=${password}`
        }).then(res => {
            if (res.status === 200) {
                resolve(res.json());
            } else {
                reject(res.status);
            }
        }).catch(err => reject(err)))
    }

    getUserByEmail(email) {
        return fetchJSON(`http://localhost:3333/user/${email}`);
    }

    getByEmailToUser(email) {
        return fetchJSON(`http://localhost:3333/verifyUser/${email}`);
    }

    searchUserByEmailAndUsername(query) {
        return fetchJSON(`http://localhost:3333/searchUser/${query}`);
    }

}
