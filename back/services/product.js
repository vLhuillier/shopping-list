const ProductDAO = require("../datamodel/productdao")

module.exports = class ProductService {
    constructor(db) {
        this.dao = new ProductDAO(db);
    }

    isValid(product) {

        product.label = product.label ? product.label.trim() : "";
        if (product.label === "") return false;
        if (!(typeof product.checked === "boolean")) return false;
        if (product.quantity === '0' || product.quantity === 0 ||  product.quantity === null || product.quantity === undefined ) {
            return false;
        }
        if (product.list_id === '0' || product.list_id === 0 ||  product.list_id === null || product.list_id === undefined ) {
            return false;
        }
        if (typeof product.list_id === "string" ){
            if (!this.isNumeric(product.list_id)) return false;
        }
        else if (!(typeof product.list_id === "number")) return false;

        return true
    }

     isNumeric(str) {
        if (typeof str != "string") return false
        return !isNaN(str) &&
            !isNaN(parseFloat(str))
    }

}