module.exports = class User {
    constructor(userName, email, challenge, registrationDate) {
        this.userName = userName
        this.email = email
        this.challenge = challenge,
        this.registrationDate = registrationDate
    }

    toString() {
        return `${this.userName}`
    }
}