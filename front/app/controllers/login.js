

class LoginController extends BaseFormController {
    constructor() {
        super(true)
        this.userApi = new UserAPIService();
    }

    displayLoginForm(){

        fadeOut('register-page');
        fadeIn('login-page');
    }

    displayRegisterForm(){
        fadeOut('login-page');
        fadeIn('register-page');
    }

    async register() {
        let username = this.validateRequiredField('#registerUsername', 'Pseudo');
        let email = this.validateRequiredField('#registerEmail', 'Adresse e-mail');
        let password = this.validateRequiredField('#registerPassword', 'Mot de passe');
        let repeatPassword = this.validateRequiredField('#repeatRegisterPassword', 'Répéter le mot de passe');

        if(password !== repeatPassword){
            $('#errorRegisterPassword').setAttribute('data-error', '* Les mots de passe ne correpondent pas');
            $('#errorRepeatRegisterPassword').setAttribute('data-error', '* Les mots de passe ne correpondent pas');
            $('#registerPassword').className = 'validate invalid';
            $('#repeatRegisterPassword').className = 'validate invalid';
            this.toast("Les mots de passe ne correpondent pas.", 'warn', 7000);
        }

        let userExist = true;
        if ((username != null) && (email != null) && (password != null)  && (repeatPassword != null)) {
            const user = await this.userApi.getByEmailToUser(email).catch(res => {
                if(res === 404){
                    userExist = false;
                }else {
                    this.displayServiceError(res)
                }
            });
            if(userExist){
                $('#registerEmail').className = 'validate invalid';
                $('#errorRegisterEmail').setAttribute('data-error', '* Adresse email déjà utilisée');
                this.toast("Cette adresse email est déjà utilisée.", 'warn', 7000);
            }else{
                this.userApi.register(username, email, password)
                    .then(user => {
                        this.authenticate(email, password).then( ( res) => {
                            this.toast("Vous êtes connecté", 'success', 7000);
                            setTimeout(function(){
                                location.hash = '';
                                document.location.reload();
                            }, 1000);
                        }).catch(err => this.displayServiceError(err));
                    }).catch(err => {
                    this.displayServiceError(err)
                });
            }
        }
    }

    async login(){
        this.constants.LOADER_SELECTOR.style = "display: flex!important;";
        let email = this.validateRequiredField('#fieldLogin', 'Adresse e-mail');
        let password = this.validateRequiredField('#fieldPassword', 'Mot de passe');
        if ((email != null) && (password != null)) {
            this.authenticate(email, password).then( ( res) => {
                M.Toast.dismissAll();
                if(res){
                    this.toast("Vous êtes connecté", 'success', 7000);
                    setTimeout(function(){
                        location.hash = '';
                        document.location.reload();
                    }, 1000);
                }else{
                    this.displayServiceError();
                    document.getElementById('loader').style = "display: none!important;opacity: 0!important;";
                }
            }).catch(err => {
                this.displayServiceError(err);
                document.getElementById('loader').style = "display: none!important;opacity: 0!important;";
            });
        }
    }

    async authenticate(email, password) {

        return new Promise((resolve, reject) =>
            this.userApi.authenticate(email, password).then(res => {
                if(res.token != null ){
                    localStorage.setItem("token", res.token);
                    resolve(res.token);
                }else{
                    this.displayServiceError('');
                }
            }).catch(err => {
                if (err === 401) {
                    document.getElementById('fieldLogin').className = 'validate invalid';
                    document.getElementById('fieldPassword').className = 'validate invalid';
                    this.toast("Adresse e-mail ou mot de passe incorrect", 'warn', 7000);
                } else {
                    this.displayServiceError(err)
                }
                reject(err)
            })
        )
    }


}

window.loginController = new LoginController();
