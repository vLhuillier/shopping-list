const BaseDAO = require('./basedao')

module.exports = class ListDAO extends BaseDAO {

    constructor(db) {
        super(db,"list", "product");
    }

    insert(list) {
        let i = 0;
        return new Promise((resolve, reject) =>
            this.db.query("INSERT INTO list(checked, shop, date, archived, shared, user_id) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id",
                [list.checked, list.shop, list.date, list.archived, list.shared, list.user_id]).then(res => {
                let idReturning = res.rows[0].id;
                if(idReturning){
                    if(typeof list.products !== 'undefined' && list.products !== null &&  list.products.length > 0){
                        list.products.forEach(product => {
                            if(product.checked != null && product.label != null && product.quantity != null){
                                this.db.query("INSERT INTO product(checked, label, quantity, list_id) VALUES ($1,$2,$3,$4)",
                                    [product.checked, product.label, product.quantity, idReturning]);
                            }
                            if(i === list.products.length -1){
                                resolve(idReturning);
                            }
                            i++;
                        });
                    }else{
                        resolve(idReturning);
                    }
                }else{
                    reject();
                }
            })
            .catch(e => reject(e)));
    }

    getAll(userId) {
        return new Promise((resolve, reject) =>
            this.db.query(`SELECT *, ${this.tableList}.id as id , 
                                     ${this.tableList}.checked as checked, 
                                     ${this.tableProduct}.id as product_id, 
                                     ${this.tableProduct}.checked as product_checked,
                                     ${this.tableProduct}.label as label,
                                     ${this.tableProduct}.quantity as quantity,
                                     ${this.tableProduct}.list_id as list_id
                            FROM ${this.tableList} 
                            LEFT JOIN ${this.tableProduct} 
                            ON ${this.tableProduct}.list_id = ${this.tableList}.id 
                            WHERE user_id = ${userId} AND ${this.tableList}.shared = 'false'
                            ORDER BY ${this.tableList}.id DESC`)
            .then(resList => {
                let lists = this.setProductsToLists(resList.rows);
                resolve(lists);
            })
            .catch(e => reject(e)))
    }

    update(list) {
        console.log(list);
        return this.db.query("UPDATE list SET checked=$1, shop=$2, date=$3, archived=$4, shared=$5, user_id=$6 WHERE id=$7",
            [list.checked, list.shop, list.date, list.archived, list.shared, list.user_id, list.id])
    }

    delete(idList) {
        return new Promise((resolve, reject) =>
            this.getById(idList).then(list => {
                this.db.query(`DELETE FROM ${this.tableList}  WHERE id=$1`,[idList]).then(ok => {
                    if(ok){
                        if(list.products ? list.products.length > 0 : false){
                            list.products.forEach(product => {
                                this.db.query(`DELETE FROM  ${this.tableProduct}  
                          WHERE list_id=$1`,
                                    [idList]);
                            });
                        }
                        resolve(list);
                    }
                }).catch(e => reject(e))
            }).catch(e => reject(e))
        )
    }

    getById(id) {
        return new Promise((resolve, reject) =>
            this.db.query(`SELECT *, ${this.tableList}.id as id , 
                                     ${this.tableList}.checked as checked, 
                                     ${this.tableProduct}.id as product_id, 
                                     ${this.tableProduct}.checked as product_checked,
                                     ${this.tableProduct}.label as label,
                                     ${this.tableProduct}.quantity as quantity,
                                     ${this.tableProduct}.list_id as list_id
                            FROM ${this.tableList} 
                            LEFT JOIN ${this.tableProduct} 
                            ON ${this.tableProduct}.list_id = ${this.tableList}.id 
                            WHERE ${this.tableList}.id=$1`
                , [ id ])
                .then(res =>{
                    let lists = this.setProductsToLists(res.rows);
                    resolve(lists[0])
                })
                .catch(e => { console.log(e), reject(e)}));
    }

    getAllListsArchived(userId) {
        return new Promise((resolve, reject) =>
            this.db.query(`SELECT *, ${this.tableList}.id as id , 
                                     ${this.tableList}.checked as checked, 
                                     ${this.tableProduct}.id as product_id, 
                                     ${this.tableProduct}.checked as product_checked,
                                     ${this.tableProduct}.label as label,
                                     ${this.tableProduct}.quantity as quantity,
                                     ${this.tableProduct}.list_id as list_id
                            FROM ${this.tableList} 
                            LEFT JOIN ${this.tableProduct} 
                            ON ${this.tableProduct}.list_id = ${this.tableList}.id 
                            WHERE user_id = ${userId}
                            AND archived = true
                            ORDER BY ${this.tableList}.id DESC`)
                .then(resList => {
                    console.log(resList)
                    let lists = this.setProductsToLists(resList.rows);
                    resolve(lists);
                })
                .catch(e => reject(e)))
    }

}