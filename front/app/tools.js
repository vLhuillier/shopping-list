function $(selector, f) {
    if (f == undefined)
        return document.querySelector(selector)
    else 
        document.querySelectorAll(selector).forEach(f)
}

function routing(path = null){
    if(path === null){
        path = window.location.hash;
    }

    if(path !== ''){
        path = path.replace('#', '');
        navigate(path);
    }else{
        navigate("index");
    }

}

function fetchJSON(url, token) {
    const headers = new Headers();
    if (token !== undefined) {
        headers.append("Authorization", `Bearer ${token}`)
    }
    return new Promise((resolve, reject) => fetch(url, {cache: "no-cache", headers: headers})
        .then(res => {
            if (res.status === 200) {
                resolve(res.json())
            } else if (res.status === 401) {
                document.getElementById('loader').style = "display: none!important";
                document.getElementById('modalLogin').style = "display: flex!important;opacity: 1!important;";
            }else {
                reject(res.status)
            }
        })
        .catch(err => reject(err)))
}

function include(selector, url, urlcontroller) {
    fetch(url, {cache: "no-cache"})
        .then(res => res.text())
        .then(html => {
            $(`#${selector}`).innerHTML = html
            fetch (urlcontroller, {cache: "no-cache"})
                .then(res => res.text())
                .then(js => {
                    if(js === '<!doctype html><title>404 Not Found</title><h1 style="text-align: center">404 Not Found</h1>'){
                        let content = document.getElementById('content');
                        content.innerHTML = "" +
                            '<div class="row sur-container">' +
                                '<div class="input-field col s2 text-align-center">' +
                                    '<i class="material-icons prefix">error_outline</i>' +
                                '</div>' +
                                '<div class="input-field col s10">' +
                                    '<h4 class="subtitlePage">404 ERROR</h4>' +
                                '</div>' +
                            '</div>' +
                            "<div class='container-404 row'>" +
                                "<div class='col s6'> "+
                                    "<p class='text-error-404'>Mesdames et Messieurs les soi-disant internautes ,</p>" +
                                    "<p class='text-error-404'>J'ai un dernier argument dont vous devrez tenir compte… Membres de ce prétendu World Wide Web...</p>" +
                                    "<p class='text-error-404'><strong>Voici Chewbacca !</strong></p>" +
                                    "<p class='text-error-404'>Chewbacca est un wookiee de la planète Kashyyyk et Chewbacca réside sur la planète Endor… </p>" +
                                    "<p class='text-error-404'>Si l'on y réfléchit cela n'a aucun sens, nous sommes d'accord ?</p>" +
                                    "<p class='text-error-404'><strong>Pourquoi un wookiee de deux mètres quarante, une taille imposante,choisit-il de vivre sur Endor en compagnie de tout petits Ewoks ? </strong> Tout ceci n'a aucun sens, nous sommes d'accord ! </p>" +
                                    "<p class='text-error-404'>Mais la première question que vous devez vous poser c'est : </p>" +
                                    "<p class='text-error-404 italic'>« Qu'est-ce que ceci a à voir avec cette erreur 404 ? » </p>" +
                                    "<p class='text-error-404'>Rien du tout ! Mesdames et Messieurs ceci n'a rien à voir avec cette erreur 404 ! Ça n'a absolument aucun sens ! Regardez, vous venez de tapez <strong>n'importe quoi dans l'url </strong> et je viens vous parler de Chewbacca ! </p>" +
                                "</div>" +
                                "<div class='col s6'><div class='bse_container'></div></div>" +
                                "<div class='col s10'> "+
                                    "<p class='text-error-404'>Cela a-t-il un sens ? Mesdames et Messieurs ce que je vous dis n'a aucun sens ! Rien de tout cela n'a de sens alors demandez-vous lorsque vous serez réunis pour délibérer afin d'établir en votre âme et conscience votre verdict : </p>" +
                                    "<p class='text-error-404 italic'>« TOUT CELA A-T-IL UN SENS ? » </p>" +
                                    "<p class='text-error-404'>Non ! Mesdames et Messieurs les soi-disant internautes </p><br>" +
                                    "<p class='text-error-404'>Si Chewbacca vit sur <strong>Endor</strong> vous devez retourné sur la <strong> page d'accueil </strong> ! J'en ai terminé</p>" +
                                "</div>" +
                                "<div class='col s2'> "+
                                    '<a onclick="navigate(`index`)" class="btn-floating btn-large right custom-bg-color2 tooltipped" data-position="left" data-tooltip="Retour à l\'accueil">' +
                                        '<i class="large material-icons">undo</i>' +
                                    '</a>' +
                                "</div>" +
                            "</div>" ;
                        content.style = "background-color: transparent;color: white";

                    }else{
                        eval(js)
                    }

                })
        })
        .catch(function(err) {
            console.log('Failed to fetch page: ', err)
        });
}

function fadeIn(id){
    let el = document.getElementById(id);
    el.classList.add('show');
    el.classList.remove('hide');
}

function fadeOut(id){
    let el = document.getElementById(id);
    el.classList.add('hide');
    el.classList.remove('show');
}

function navigate(view) {
    let path = view;
    include('content',  `views/${view}.html`, `app/controllers/${view}.js`);

    let activesLink = document.getElementsByClassName('link');
    for (let activesLinkElement of activesLink) {
        activesLinkElement.className  = 'waves-effect link';
    }
    if(view === "index"){
        document.getElementById('index-link').className = 'active waves-effect link';
    }
    else if(view === "history"){
        document.getElementById('history-link').className = 'active waves-effect link';
    }
    else if(view === "account"){
        document.getElementById('account-link').className = 'active waves-effect link';
    }
}

const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;
function reviver(key, value) {
    if (typeof value === "string" && dateFormat.test(value)) {
        return new Date(value);
    }
    return value;
}

function getParameterByName(name) {
    let match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}