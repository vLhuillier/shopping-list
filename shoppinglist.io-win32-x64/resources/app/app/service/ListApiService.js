const BaseUrl = "http://localhost:3333";

class ListApiService extends BaseAPIService{

    constructor() {
        super("list")
    }

    checkAuthentication(token) {
        if(this.token === null || this.token === undefined && token !== null && token !== undefined){
            this.token = token;
        }
        return fetchJSON(`http://localhost:3333/user/checkAuthentication/${token}`, this.token);
    }

    getAll(userId) {
        return fetchJSON('http://localhost:3333/lists/' + userId, this.token)
    }

    getAllListsArchived(userId) {
        return fetchJSON('http://localhost:3333/listsArchived/' + userId, this.token)
    }

    get(id) {
        return fetchJSON(`${this.url}/${id}`, this.token)
    }

    delete(id) {
        this.headers.delete('Content-Type')
        return fetch(`${this.url}/${id}`, { method: 'DELETE', headers: this.headers })
    }

    insert(list) {
        this.headers.set( 'Content-Type', 'application/json' )
        return new Promise((resolve, reject) =>
             fetch(this.url, {
                method: 'POST',
                 headers: this.headers,
                body: JSON.stringify(list)
            }).then(res => {
                resolve(res);
             }).catch(e => reject(e))
        )
    }

    insertWithIdReturned(list) {
        this.headers.set( 'Content-Type', 'application/json' )
        return new Promise((resolve, reject) =>
            fetch(this.url, {
                method: 'POST',
                headers: this.headers,
                body: JSON.stringify(list)
            }).then(res => {
                res.json().then(body => { resolve(body); });

            }).catch(e => reject(e))
        )
    }

    update(list) {
        this.headers.set( 'Content-Type', 'application/json' )
        return fetch(this.url, {
            method: 'PUT',
            headers: this.headers,
            body: JSON.stringify(list)
        })
    }

}