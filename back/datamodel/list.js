module.exports = class List {
    constructor(checked, shop, date, archived, shared, user_id, products = Array()) {
        this.checked = checked;
        this.shop = shop;
        this.date = date;
        this.archived = archived;
        this.shared = shared;
        this.user_id = user_id;
        this.products = products;
    }
    toString() {
        return `${this.shop} ${this.date}`
    }
}