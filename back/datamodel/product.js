module.exports = class Product {
    constructor(id, checked, label, quantity, list_id) {
        this.id = id;
        this.checked = checked;
        this.label = label;
        this.quantity = quantity;
        this.list_id = list_id;
    }
    toString() {
        return `${this.label} x${this.quantity}`
    }
}