module.exports = (app, listService, userService, sharingService, jwt) => {

    /* Ajoute une liste partagée */
    app.post("/listsSharingByUser", jwt.validateJWT, async (req, res) => {
        try {
            const share = req.body;
            if (!sharingService.isValid(share))  {
                return res.status(400).end();
            }

            listService.dao.getById(share.list.id).then(list => {
                if(list) {

                    userService.dao.getById(share.user_id).then(user => {
                        if (!user) {
                            return res.status(400).end();
                        }
                    }).catch(e => {
                        res.status(400).end()
                    });

                    userService.dao.getById(share.owner).then(owner => {
                        if (owner) {
                            sharingService.dao.insert(share)
                                .then(res.status(200).end())
                                .catch(e => {
                                    res.status(500).end();
                                });
                        } else {
                            return res.status(400).end();
                        }
                    }).catch(e => {
                        res.status(400).end()
                    })
                }else{
                    res.status(400).end()
                }
            }).catch(e => {
                res.status(400).end();
            });
        } catch (e) {res.status(400).end(); };

    });

    /* Récupére toutes les listes partagées pour un utilisateur */
    app.get("/listsSharingByUser/:userId", jwt.validateJWT, async (req, res) => {
        try {
            const user = await userService.dao.getById(req.params.userId);
            if(user != null){
                const sharingLists = await sharingService.dao.getAllByUserId(req.params.userId)
                if (sharingLists === undefined ) {
                    return res.status(404).end();
                }
                return res.json(sharingLists);
            }else{
                res.status(404).end();
            }

        } catch (e) {
            if(e === 'not found'){
                res.status(404).end()
            }
            res.status(400).end();
        };
    });

    /* Récupére une liste partagée */
    app.get("/listsSharing/:id", jwt.validateJWT, async (req, res) => {
        try {
            const sharingList = await sharingService.dao.getById(req.params.id);
            if (sharingList === undefined) {
                return res.status(404).end();
            }
            return res.json(sharingList);
        } catch (e) {
            if(e === 'not found'){
                res.status(404).end()
            }
            res.status(400).end()
        };
    });

    /* Supprime une liste partagée pour un utillisateur */
    app.delete("/listsSharingByUser/:listId/:userId", jwt.validateJWT, async (req, res) => {
        try {
            listService.dao.getById(req.params.listId).then(list => {
                if(list) {
                    userService.dao.getById(req.params.userId).then(user => {
                        if (user) {
                            sharingService.dao.deleteUserFromSharedList(req.params.listId, req.params.userId)
                                .then(res.status(200).end())
                                .catch(e => {
                                    res.status(500).end();
                                });
                        } else {
                            return res.status(400).end();
                        }
                    }).catch(e => {
                        res.status(400).end()
                    })
                }
            }).catch(e => {
                if(e === 'not found'){
                    res.status(404).end()
                }
                res.status(400).end()
            });

        } catch (e) {
            if(e === 'not found'){
                res.status(404).end()
            }
            res.status(400).end()
        };
    });

    /* Supprime une liste partagée (tous les utilisateurs) */
    app.delete("/listsSharing/:id", jwt.validateJWT, async (req, res) => {
        try {
            const share = await sharingService.dao.getById(req.params.id);
            if (share === undefined) {
                return res.status(404).end();
            }
            sharingService.dao.delete(req.params.id)
                .then(res.status(200).end())
                .catch(e => {
                    if(e === 'not found'){
                        res.status(404).end()
                    }
                    res.status(400).end()
                });
        } catch (e) {
            if(e === 'not found'){
                res.status(404).end()
            }
            res.status(400).end()
        };
    });

    /* Met à jour une permission d'une liste partagée */
    app.put("/listsSharing", jwt.validateJWT, async (req, res) => {
        const share = req.body;
        if ((share === undefined) || (share == null)) {
            return res.status(400).end();
        }

        if ((share.id === undefined) || (share.id == null) || (!sharingService.isValid(share))) {
            return res.status(400).end();
        }

        if (await sharingService.dao.getById(share.id) === undefined) {
            return res.status(404).end();
        }

        sharingService.dao.updatePermission(share)
            .then(res.status(200).end())
            .catch(e => {
                res.status(500).end();
            });

    });

}
