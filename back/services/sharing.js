const SharingDAO = require("../datamodel/sharingdao")
const UserService = require("../services/user")

module.exports = class SharingService {
    constructor(db) {
        this.dao = new SharingDAO(db);
    }

    isValid(share) {
        if (!(typeof share.list.id === "number")) return false;
        if (!(typeof share.user_id === "number")) return false;
        if (!(typeof share.owner === "number")) return false;
        if (share.permission !== "write" && share.permission !== "read")return false;

        return true;
    }


}