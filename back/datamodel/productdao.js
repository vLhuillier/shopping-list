const BaseDAO = require('./basedao')

module.exports = class ProductDAO extends BaseDAO {

    constructor(db) {
        super(db, "list", "product");
    }

    insert(product) {
        return new Promise((resolve, reject) =>
            this.db.query("INSERT INTO product(checked, label, quantity, list_id) VALUES ($1,$2,$3,$4) RETURNING id",
                [product.checked, product.label, product.quantity, product.list_id]).then(res => {
                if(res.rows[0].id){
                    let idReturning = res.rows[0].id;
                    resolve(idReturning);
                }
            }).catch(e => reject(e)));

    }

    getAll() {
        return new Promise((resolve, reject) =>
            this.db.query(`SELECT * FROM ${this.tableProduct} ORDER BY id DESC;`)
                .then(res => resolve(res.rows))
                .catch(e => reject(e)))
    }

    getAllByListId(idList) {
        return new Promise((resolve, reject) =>
            this.db.query(`SELECT *
                            FROM ${this.tableProduct}
                            WHERE list_id = $1
                            ORDER BY ${this.tableProduct}.id DESC;`, [idList])
                .then(resProducts => {
                    resolve(resProducts.rows);
                })
                .catch(e => reject(e)))
    }

    getById(id) {
        return new Promise((resolve, reject) =>
            this.db.query(`SELECT *
                            FROM ${this.tableProduct}
                            WHERE id = $1;`, [id])
                .then(resProduct => {
                    resolve(resProduct.rows);
                })
                .catch(e => reject(e)))
    }

    update(product) {
        return this.db.query("UPDATE product SET checked=$1, label=$2, quantity=$3, list_id=$4 WHERE id=$5",
            [product.checked, product.label, product.quantity, product.list_id, product.id])
    }

    delete(idProduct) {
        return new Promise((resolve, reject) =>
            this.db.query(`DELETE FROM ${this.tableProduct}  WHERE id=$1`,[idProduct]).then(ok => {
                resolve(ok);
            }).catch(e => reject(e))
        )
    }

}