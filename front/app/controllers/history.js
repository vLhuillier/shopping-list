
class HistoryController extends BaseController {

    constructor() {
        super();
        this.displayAllListsArchived();
        this.listsArchived;
        this.constants = new Constants();
        this.stepperRestore = document.querySelector('#stepperRestore');
        this.stepperRestoreInstance = new MStepper(this.stepperRestore, {
            firstActive: 0,
            autoFocusInput: false,
            showFeedbackPreloader: true,
            autoFormCreation: true,
            stepTitleNavigation: true,
            feedbackPreloader: '<div class="spinner-layer spinner-green-only">...</div>'
        });
    }

    async displayAllListsArchived() {
        this.constants.LOADER_SELECTOR.style = "display: flex!important;";
        await this.checkAuthentication().then( () => {
            if(this.user !== null && this.user !== undefined ) {
                let html = "";
                let index = 0;
                this.model.getAllListsArchived(this.user.id).then(lists => {
                    this.listsArchived = lists;
                    if(lists.length === 0){
                        html = "<h6 class='text-empty-list'>Aucunes Listes</h6>";
                    }else {
                        for (let list of lists) {
                            let checked;
                            if (list.checked) {
                                checked = `<input type="checkbox" checked="${checked}" disabled="disabled" class="filled-in"/>`;
                            } else {
                                checked = `<input type="checkbox" disabled="disabled" class="filled-in"/>`;
                            }
                            let date = list.date ? list.date.toLocaleDateString() : 'Erreur date';
                            let productsHtml = this.generateLiProductHtml(list.products);
                            html += `<tr id="list${list.id}">
                                <td class="padding-left-20px">
                                    <p>
                                      <label>
                                        ${checked}
                                        <span></span>
                                      </label>
                                    </p>
                                </td>
                                <td>${list.shop}</td>
                                <td>${date}</td>
                                <td>${productsHtml}</td>
                                <td class="text-align-center">
                                    <i onclick="historyController.modalList();historyController.setValuesModalList('${list.id}')" class="material-icons custom-color pointer">remove_red_eye</i>
                                    <i onclick="historyController.modalRestoreList('${list.id}', 'fromArchivedPage')" class="material-icons orange-text pointer">restore</i>
                                 </td>`;

                            index++;
                        }
                    }
                    if(this.datatablePerso) {
                        this.destroyDataTableHisto();
                    }
                    $('#list_container').innerHTML = html;
                    this.initDataTableHisto();

                    this.constants.LOADER_SELECTOR.style = "display: none!important;";
                }).catch(e => {this.displayServiceError(e);this.constants.LOADER_SELECTOR.style = "display: none!important;";});
            }else{
                this.displayServiceError(401);
                this.constants.LOADER_SELECTOR.style = "display: none!important;";
                this.constants.MODAL_LOGIN_SELECTOR.style = "display: flex!important;opacity: 1!important;";
            }
        }).catch(e => {
            this.displayServiceError(e);
            this.constants.LOADER_SELECTOR.style = "display: none!important;";
            this.constants.MODAL_LOGIN_SELECTOR.style = "display: flex!important;opacity: 1!important;";
        });
    }



    async submitRestoreList(){
        if(this.checkDateInput()){
            const index = $('#confirmRestoreButton').getAttribute('data-id');
            let list =  this.listsArchived.filter(function(item) {
                return item.id == index;
            });
            if(list !== null && list !== undefined && list[0] !== null && list[0] !== undefined) {
                list = list[0];
                list.archived = false;
                list.date = this.parseDate(this.constants.LIST_DATE_RESTORE_INPUT_SELECTOR.value);
                await this.model.updateList(list).then(res => {
                    if(res !== 200 && res !== 304){
                        this.displayServiceError(res);
                    }else{
                        this.displayAllListsArchived();
                        this.toast('Liste ' + list.shop +' restaurée', 'success');
                    }
                });
            }else{
                this.toast('Erreur', 'warn');
            }
            await this.displayAllListsArchived();
            this.getModal("#restoreListModal").close();
        }
    }


    modalList() {
        this.resetFormValues();
        let height = window.outerHeight;
        let width = window.outerWidth;
        const addListModal =  document.getElementById('addListModal');
        this.constants.PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('data-mode', '');
        this.constants.PRODUCT_ADD_BUTTON_SELECTOR.setAttribute('data-id', '');
        if(this.stepper[0] !== null && this.stepper[0] !== undefined){
            this.stepper[0].style.minHeight = height * 0.65 + 'px';
        }
        if(height > width){
            addListModal.style.minHeight = height * 0.80 + 'px';
            addListModal.style.minWidth = width * 0.90 + 'px';
        }else{
            addListModal.style.minHeight = height * 0.83 + 'px';
            addListModal.style.minWidth = width * 0.85 + 'px';
        }
        this.getModal("#addListModal").open();
        document.getElementById('stepContent1').style.height = 'unset';
        $('#firstStepLi').className = "step active";
    }

    setValuesModalList(index){

        let list =  this.listsArchived.filter(function(item) {
            return item.id == index;
        });
        if(list !== null && list !== undefined && list[0] !== null && list[0] !== undefined) {
            list = list[0];
            let listName = list.shop ? list.shop : 'Erreur';
            let listDate = list.date ? list.date.toLocaleDateString() : 'Erreur date';

            this.constants.LIST_NAME_INPUT_SELECTOR.value = listName;
            this.constants.LIST_DATE_INPUT_SELECTOR.value = listDate;

            if($('#listNameLabel').className !== 'active'){
                $('#listNameLabel').className += "active";
            }
            if($('#listQuantityLabel').className !== 'active'){
                $('#listQuantityLabel').className += "active";
            }
            let htmlTitle = '<div class="input-field col s12"> <i class="material-icons prefix ">restore</i> <a class="title-header-modal" href="#" >Liste archivée: ' + listName + '</a> </div>';
            $('#titleHeaderAddList').innerHTML = htmlTitle;
            let productsHtmlTable = '';
            list.products.forEach(product => {
                let checked = false;
                if(product.checked){
                    checked = `<input type="checkbox" checked="${checked}" aria-disabled="true" disabled="disabled" class="filled-in"/>`;
                }else{
                    checked = `<input type="checkbox" aria-disabled="true" disabled="disabled" class="filled-in"/>`;
                }
                productsHtmlTable +=
                    '<tr>' +
                    '<td>' +
                    '<p>' +
                    '<label>' +
                    checked +
                    '<span></span>' +
                    '</label>' +
                    '</p>' +
                    '</td>' +
                    '<td>' + product.label + '</td>' +
                    '<td> <div class="round-quantity">' + product.quantity + '</div></td>' +
                    '<td>' +
                    '<i class="material-icons" aria-disabled="true"  style="color: #bdbdbd;" disabled="disabled">' +
                    'delete' +
                    '</i>' +
                    '</td>' +
                    '<tr>';
                index++;
            });
            this.constants.LIST_PRODUCT_CONTAINER_SELECTOR.innerHTML = productsHtmlTable;

            this.constants.LIST_NAME_INPUT_SELECTOR.setAttribute('disabled','disabled');
            this.constants.LIST_DATE_INPUT_SELECTOR.setAttribute('disabled','disabled');
            document.getElementById('listBackButton').className = 'btn-floating btn btn-flat previous-step custom-bg-color2';
            $('#formProduct').style.display = 'none';
            this.constants.LIST_ADD_BUTTON_SELECTOR.style.display = 'none';
            $('#secondStepLi').className = "step";


        }else{
            this.toast('Erreur', 'warn');
        }

    }

}

window.historyController = new HistoryController()
