const BaseDAO = require('./basedao')
const List = require("./list");
const Sharing = require("./sharing");

module.exports = class SharingDAO extends BaseDAO {

    constructor(db) {
        super(db, "list", "product", "sharing");
    }


    insert(share) {
        return new Promise((resolve, reject) =>
            this.db.query("INSERT INTO sharing(list_id, user_id, owner, permission) VALUES ($1,$2,$3,$4) RETURNING id",
                [share.list.id, share.user_id, share.owner, share.permission]).then(res => {
                let usersId = res.rows[0].id;
                    resolve(usersId);
            }).catch(e => reject(e)))
    }

    /* Listes partagées avec un utilisateur */
    getAllByUserId(userId) {
        let lists = Array();
        return new Promise((resolve, reject) =>
            this.db.query(`SELECT DISTINCT ON (sharing.list_id) *, sharing.id as shared_list_id
                            FROM sharing 
                            LEFT JOIN useraccount ON useraccount.id  = sharing.user_id
                            LEFT JOIN list ON list.id  = sharing.list_id
                            WHERE sharing.user_id = ${userId} OR sharing.owner = ${userId}`)
                .then(sharedLists => {

                    if(sharedLists.rows != null && sharedLists.rows.length !== 0){
                        for (let i = 0; i < sharedLists.rows.length; i++) {

                            sharedLists.rows[i].id = sharedLists.rows[i].shared_list_id;
                            delete sharedLists.rows[i].shared_list_id;

                            let permission;
                            if(userId === sharedLists.rows[i].owner){
                                permission = "write";
                            }else{
                                permission = sharedLists.rows[i].permission;
                            }

                            this.db.query(`SELECT *, ${this.tableList}.id as id , 
                                     ${this.tableList}.checked as checked, 
                                     ${this.tableProduct}.id as product_id, 
                                     ${this.tableProduct}.checked as product_checked,
                                     ${this.tableProduct}.label as label,
                                     ${this.tableProduct}.quantity as quantity,
                                     ${this.tableProduct}.list_id as list_id
                                    FROM ${this.tableList} 
                                    LEFT JOIN ${this.tableProduct} 
                                    ON ${this.tableProduct}.list_id = ${this.tableList}.id 
                                    WHERE ${this.tableList}.id = ${sharedLists.rows[i].list_id}
                                    ORDER BY ${this.tableList}.id DESC`)
                                    .then(resList => {

                                        if(resList) {
                                            let resProducts = this.setProductsToLists(resList.rows);
                                            if (resProducts) {
                                                this.setUsersToLists(resProducts).then(resUsers => {
                                                    if (resUsers) {
                                                        this.setOwnersToLists(resUsers).then(resOwners => {

                                                            if (resOwners) {
                                                                resOwners = this.removeDuplicateLists(resOwners);

                                                                resOwners = this.setPermissionToList(resOwners, permission);

                                                                const list = Object.assign(new List(), resOwners);
                                                                delete list.users;
                                                                delete list.owner;
                                                                delete list.permission;

                                                                const sharedList = new Sharing(list, resOwners.users, resOwners.owner, resOwners.permission);
                                                                sharedList.id = sharedLists.rows[i].id;
                                                                sharedList.users = resOwners.users;
                                                                delete sharedList.user_id;

                                                                lists.push(sharedList);

                                                                if(lists.length === sharedLists.rows.length){
                                                                    resolve(lists);
                                                                }
                                                            }else{
                                                                reject('error Owners');
                                                            }
                                                        }).catch(e => reject(e));
                                                    }else{
                                                        reject('error Users');
                                                    }
                                                }).catch(e => reject(e));
                                            }else{
                                                reject('error Products');
                                            }
                                        }
                                    }).catch(e => reject(e));
                        }
                    }else{
                        resolve([]);
                    }


            }).catch(e => reject(e)));

    }

    updatePermission(share) {
        return this.db.query("UPDATE sharing SET list_id=$1, user_id=$2, owner=$3, permission=$4  WHERE id=$6",
            [share.list_id, share.user_id, share.owner, share.permission, share.id])
    }

    delete(idShare) {
        return new Promise((resolve, reject) =>
            this.getById(idShare).then(share => {
                this.db.query(`DELETE FROM ${this.tableSharing}  WHERE id=$1`,[idShare]).then(ok => {
                    if(ok){
                        resolve(share);
                    }
                }).catch(e => reject(e))
            }).catch(e => reject(e))
        )
    }

    deleteUserFromSharedList(listId, userId) {
        return new Promise((resolve, reject) =>
            this.db.query(`DELETE FROM ${this.tableSharing}  WHERE list_id=$1 AND user_id=$2`,[listId, userId]).then(ok => {
                    resolve(ok);
            }).catch(e => reject(e))
        )
    }

    getById(id) {
        return new Promise((resolve, reject) =>
            this.db.query(`SELECT *
                            FROM sharing
                            LEFT JOIN list ON list.id = sharing.list_id
                            LEFT JOIN useraccount ON useraccount.id = sharing.user_id
                            WHERE sharing.id = $1;`, [id])
                .then(res => {
                    console.log(res.rows);
                    if(res.rows.length !== 0){
                        this.db.query(`SELECT *, ${this.tableList}.id as id , 
                                     ${this.tableList}.checked as checked, 
                                     ${this.tableProduct}.id as product_id, 
                                     ${this.tableProduct}.checked as product_checked,
                                     ${this.tableProduct}.label as label,
                                     ${this.tableProduct}.quantity as quantity,
                                     ${this.tableProduct}.list_id as list_id
                                    FROM ${this.tableList} 
                                    LEFT JOIN ${this.tableProduct} 
                                    ON ${this.tableProduct}.list_id = ${this.tableList}.id 
                                    WHERE ${this.tableList}.id = ${res.rows[0].list_id}
                                    ORDER BY ${this.tableList}.id DESC`)
                            .then(resList => {

                                if(resList) {
                                    let resProducts = this.setProductsToLists(resList.rows);
                                    if (resProducts) {
                                        this.setUsersToLists(resProducts).then(resUsers => {
                                            if (resUsers) {
                                                this.setOwnersToLists(resUsers).then(resOwners => {
                                                    if (resOwners) {
                                                        resOwners = this.removeDuplicateLists(resOwners);

                                                        const list = Object.assign(new List(), resOwners[0]);
                                                        delete list.users;
                                                        delete list.owner;
                                                        delete list.permission;

                                                        const sharedList = new Sharing(list, resOwners[0].users, resOwners[0].owner, resOwners[0].permission);
                                                        sharedList.id = id;
                                                        sharedList.users = resOwners[0].users;
                                                        delete sharedList.user_id;
                                                        resolve(sharedList)
                                                    }else{
                                                        reject('error Owners');
                                                    }
                                                }).catch(e => reject(e));
                                            }else{
                                                reject('error Users');
                                            }
                                        }).catch(e => reject(e));
                                    }else{
                                        reject('error Products');
                                    }
                                }
                            }).catch(e => reject('not found'));
                    }else{
                        reject('not found');
                    }

                })
                .catch(e => reject(e)));
    }

    getAllListsArchived(userId) {
        return new Promise((resolve, reject) =>
            this.db.query(`SELECT *, ${this.tableList}.id as id , 
                                     ${this.tableList}.checked as checked, 
                                     ${this.tableProduct}.id as product_id, 
                                     ${this.tableProduct}.checked as product_checked,
                                     ${this.tableProduct}.label as label,
                                     ${this.tableProduct}.quantity as quantity,
                                     ${this.tableProduct}.list_id as list_id
                            FROM ${this.tableList} 
                            LEFT JOIN ${this.tableProduct} 
                            ON ${this.tableProduct}.list_id = ${this.tableList}.id 
                            WHERE user_id = ${userId}
                            AND archived = true
                            ORDER BY ${this.tableList}.id DESC`)
                .then(resList => {
                    let lists = this.setProductsToLists(resList.rows);
                    resolve(lists);
                })
                .catch(e => reject(e)))
    }

    removeDuplicateLists(lists){
        if(lists !== undefined && lists !== null ){
            /* Supprime les listes en doublons */
            const seen = new Set();
            let newLists = lists.filter(el => {
                const duplicate = seen.has(el.id);
                seen.add(el.id);
                return !duplicate;
            });
            return newLists;
        }
    }

    removeDuplicateUsers(users){
        if(users !== undefined && users !== null ){
            /* Supprime les users en doublons */
            const seen = new Set();
            let newUsers = users.filter(el => {
                const duplicate = seen.has(el.id);
                seen.add(el.id);
                return !duplicate;
            });
            return newUsers;
        }
    }

    setOwnersToLists(lists){
        if(lists){
            return new Promise((resolve, reject) =>
                lists.forEach(list => {
                    list.users = this.removeDuplicateUsers(list.users);
                    if(typeof list.owner === 'undefined' || list.owner === null){
                        list.owner = "";
                    }
                    this.db.query(`SELECT useraccount.id, useraccount.username, useraccount.email
                                    FROM sharing 
                                    LEFT JOIN useraccount ON useraccount.id  = sharing.owner
                                    WHERE sharing.list_id = ${list.id}`)
                        .then(users => {
                            if(users.rows.length > 0){
                                list.owner = users.rows[0];
                            }
                            resolve(lists)
                        }).catch(e => reject(e));
                }),
            );
        }
    }

    setPermissionToList(list, permission){

        if(typeof list[0].permission === 'undefined' || list[0].permission === null){
            list[0].permission = permission;
        }

        if(list[0].permission !== 'write' && list[0].permission !== 'read' ){
            list[0].permission = permission;
        }
        return list[0];
    }

    setUsersToLists(lists){

        if(lists){
            let y = 0;
            const listLentgth = lists.length;
            if(lists.length === 1){
                if(typeof lists[0].users === 'undefined' || lists[0].users === null){
                    lists[0].users = Array();
                }
                return new Promise((resolve, reject) =>
                    this.db.query(`SELECT useraccount.id, useraccount.username, useraccount.email, sharing.permission
                                FROM sharing 
                                LEFT JOIN useraccount ON useraccount.id = sharing.user_id
                                WHERE sharing.list_id = ${lists[0].id}`)
                        .then(users => {
                            if(users.rows.length > 0){
                                lists[0].users = users.rows
                                resolve(lists);
                            }else{
                                resolve(lists);
                            }
                            if(listLentgth === y ) {
                                resolve(lists);
                            }
                        }).catch(e => reject(e))
                );

            }else{
                return new Promise((resolve, reject) =>
                    lists.forEach(list => {
                        if(typeof list.users === 'undefined' || list.users === null){
                            list.users = Array();
                        }
                        this.db.query(`SELECT useraccount.id, useraccount.username, useraccount.email
                                    FROM sharing 
                                    LEFT JOIN useraccount ON useraccount.id = sharing.user_id
                                    WHERE sharing.list_id = ${list.id}`)
                            .then(users => {

                                if(users.rows.length > 0){
                                    for (let i = 0; i < users.rows.length; i++) {
                                        list.users.push(users.rows[i]);

                                        if(listLentgth === y ) {
                                            resolve(lists);
                                        }
                                    }
                                }else{
                                    resolve(lists);
                                }
                                if(listLentgth === y ) {
                                    resolve(lists);
                                }
                            }).catch(e => reject(e));
                        y++;
                    }),
                );
            }
        }

    }

}