const BaseDAO = require('./basedao')

module.exports = class UserDAO extends BaseDAO {
    constructor(db) {
        super(db, "user")
    }
    insert(user) {
        return new Promise((resolve, reject) =>
            this.db.query("INSERT INTO useraccount(username, email, challenge, registrationDate) VALUES ($1,$2,$3,$4) RETURNING id",
                [user.userName, user.email, user.challenge, user.registrationDate]).then(res => {
                let idReturning = res.rows[0].id;
                if(idReturning){
                    this.db.query("SELECT * FROM useraccount WHERE id=$1", [ idReturning ])
                        .then(res => resolve(res.rows[0]) )
                        .catch(e => reject(e));
                }
            }).catch(e => reject(e)));
    }

    getByEmail(email) {
        return new Promise((resolve, reject) =>
            this.db.query("SELECT * FROM useraccount WHERE email=$1", [ email ])
                .then(res => resolve(res.rows[0]) )
                .catch(e => reject(e)))
    }

    getByEmailToUser(email) {
        return new Promise((resolve, reject) =>
            this.db.query("SELECT id, email, username FROM useraccount WHERE email=$1", [ email ])
                .then(res => resolve(res.rows[0]) )
                .catch(e => reject(e)))
    }

    searchByEmailAndUsername(query) {
        query = "%"+query+"%";
        return new Promise((resolve, reject) =>
            this.db.query("SELECT id, email, username FROM useraccount WHERE email LIKE $1 OR username LIKE $1", [ query ])
                .then(res => resolve(res.rows) )
                .catch(e => reject(e)))
    }

    getById(id) {
        return new Promise((resolve, reject) =>
            this.db.query("SELECT * FROM useraccount WHERE id=$1", [ id ])
                .then(res => resolve(res.rows[0]) )
                .catch(e => reject(e)))
    }

}