class BaseFormController extends BaseController {

    constructor(secured) {
        super(secured)
    }

    validateRequiredField(selector, name) {
        const value =  $(selector).value
        if ((value == null) || (value === "")) {
            this.toast(`Le champs '${name}' est obligatoire`, 'warn');
            $(selector).setAttribute('data-error', '* Champ obligatoire');
            $(selector).className = 'validate invalid';
            return null
        }else{
            $(selector).className = 'validate valid';
        }
        return value
    }

}
