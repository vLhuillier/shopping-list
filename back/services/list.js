const ListDAO = require("../datamodel/listdao")

module.exports = class ListService {
    constructor(db) {
        this.dao = new ListDAO(db);
    }

    isValid(list) {
        if (!(typeof list.checked === "boolean")) return false;
        list.shop = list.shop ? list.shop.trim() : "";
        if (list.shop === "") return false;
        if (list.date != null) {
            if (list.date instanceof String) {
                list.date = new Date(list.date)
            }
            if (list.date >= new Date()) return false
        }
        if (!(typeof list.archived === "boolean")) return false;

        return true
    }

}