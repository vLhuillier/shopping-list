module.exports = (app, listService, productService, jwt) => {

    /* Récupére toutes les listes archivées */
    app.get("/listsArchived/:userId", jwt.validateJWT, async (req, res) => {
        try {
            const list = await listService.dao.getAllListsArchived(req.params.userId)
            if (list === undefined) {
                return res.status(404).end();
            }
            return res.json(list);
        } catch (e) { res.status(400).end() };
    });

    /* Récupére toutes les listes */
    app.get("/lists/:userId", jwt.validateJWT, async (req, res) => {
        try {
            const list = await listService.dao.getAll(req.params.userId)
            if (list === undefined) {
                return res.status(404).end();
            }
            return res.json(list);
        } catch (e) { res.status(400).end() };
    })

    /* Récupére une liste */
    app.get("/list/:id", jwt.validateJWT, async (req, res) => {
        try {
            const list = await listService.dao.getById(req.params.id)
            if (list === undefined) {
                return res.status(404).end();
            }
            return res.json(list);
        } catch (e) { res.status(400).end() };
    });

    /* Ajoute une liste */
    app.post("/list", jwt.validateJWT, (req, res) => {
        const list = req.body;
        if (!listService.isValid(list))  {
            return res.status(400).end();
        }
        listService.dao.insert(list)
            .then(idList => {
                res.status(200);
                res.setHeader('Content-Type', 'application/json');
                res.end(JSON.stringify({ id: idList.toString() }));
            })
            .catch(e => {
                res.status(500).end();
            });
    });

    /* Supprime une liste */
    app.delete("/list/:id", jwt.validateJWT, async (req, res) => {
        const list = await listService.dao.getById(req.params.id);
        if (list === undefined) {
            return res.status(404).end();
        }
        listService.dao.delete(req.params.id)
            .then(res.status(200).end())
            .catch(e => {
                res.status(500).end();
            });
    });

    /* Met à jour une liste */
    app.put("/list", jwt.validateJWT, async (req, res) => {
        const list = req.body;
        if ((list === undefined) || (list == null)) {
            return res.status(400).end();
        }

        if ((list.id === undefined) || (list.id == null) || (!listService.isValid(list))) {
            return res.status(400).end();
        }

        if (await listService.dao.getById(list.id) === undefined) {
            return res.status(404).end();
        }

        const products = list.products;
        delete list.products;
        listService.dao.update(list)
            .then(res.status(200).end())
            .catch(e => {
                res.status(500).end();
            });

        productService.dao.update(products)
            .then(res.status(200).end())
            .catch(e => {
                res.status(500).end();
            });
    });

}
