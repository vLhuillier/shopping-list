const List = require('./list')
const Product = require('./product')
const Sharing = require('./sharing')

module.exports = (listService, productService, userService, sharingService) => {
    return new Promise(async (resolve, reject) => {
        try {
            await userService.dao.db.query("CREATE TABLE UserAccount(id SERIAL PRIMARY KEY, username TEXT NOT NULL, email TEXT NOT NULL, challenge TEXT NOT NULL, registrationdate DATE)")
            await listService.dao.db.query("CREATE TABLE List (id SERIAL PRIMARY KEY, checked BOOLEAN, shop TEXT, date DATE, archived BOOLEAN, shared BOOLEAN, user_id INTEGER, FOREIGN KEY (user_id) REFERENCES UserAccount(id) ON DELETE CASCADE)")
            await productService.dao.db.query("CREATE TABLE Product (id SERIAL PRIMARY KEY, checked BOOLEAN, label TEXT, quantity NUMERIC, list_id INTEGER, FOREIGN KEY (list_id) REFERENCES List(id) ON DELETE CASCADE)")
            await sharingService.dao.db.query("CREATE TABLE Sharing (id SERIAL PRIMARY KEY, list_id INTEGER, FOREIGN KEY (list_id) REFERENCES List(id), user_id INTEGER, FOREIGN KEY (user_id) REFERENCES UserAccount(id), owner INTEGER, FOREIGN KEY (user_id) REFERENCES UserAccount(id), permission TEXT NOT NULL)")
        } catch (e) {
            if (e.code === "42P07") { // TABLE ALREADY EXISTS https://www.postgresql.org/docs/8.2/errcodes-appendix.html
                resolve()
            } else {
                reject(e)
            }
            return
        }

        try {
            let dateNow = new Date().toLocaleDateString();
            userService.insert("User1", "user1@example.com", "azerty", dateNow)
                .then(_ => userService.dao.getByEmail("user1@example.com"))
                .then(async user1 => {
                    let newProduct1 = new Product(1, false, 'Bichoco', 3, 1);
                    let newProduct2 = new Product(2, false, 'Luges', 8, 1);
                    let products = Array();
                    products.push(newProduct1);
                    products.push(newProduct2);
                    let newList = new List(
                        false,
                        "Carrefour",
                        new Date('2022-03-04').toLocaleDateString(),
                        false,
                        false,
                        user1.id,
                        products
                    );
                    listService.dao.insert(newList);
                    let newList2 = new List(
                        false,
                        "Lidl",
                        new Date('2021-09-04').toLocaleDateString(),
                        false,
                        false,
                        user1.id
                    );
                    await listService.dao.insert(newList2);
                    let newListHisto = new List(
                        false,
                        "Terrasse du port",
                        new Date('2020-09-04').toLocaleDateString(),
                        false,
                        false,
                        user1.id
                    );
                    await listService.dao.insert(newListHisto);
                })


            userService.insert("User2", "user2@example.com", "azerty", dateNow)
                .then(_ => userService.dao.getByEmail("user2@example.com"))
                .then(async user2 => {
                    let newProduct1 = new Product(1, false, 'Madeleines', 18, 1);
                    let newProduct2 = new Product(2, false, 'Igloo', 8, 1);
                    let products = Array();
                    products.push(newProduct1);
                    products.push(newProduct2);
                    let newList = new List(
                        false,
                        "Auchan",
                        new Date('2022-03-04').toLocaleDateString(),
                        false,
                        false,
                        user2.id,
                        products
                    );
                    await listService.dao.insert(newList);
                });

            userService.insert("User3", "user3@example.com", "azerty", dateNow)
                .then(_ => userService.dao.getByEmail("user3@example.com"))
                .then(async user3 => {
                    let newProduct1 = new Product(1, false, 'Chocolat', 4, 1);
                    let newProduct2 = new Product(2, false, 'Marmite', 9, 1);
                    let products = Array();
                    products.push(newProduct1);
                    products.push(newProduct2);
                    let newList = new List(
                        false,
                        "Mr.Bricolage",
                        new Date('2022-08-04').toLocaleDateString(),
                        false,
                        false,
                        user3.id,
                        products
                    );
                    await listService.dao.insert(newList);
                });

            setTimeout( function(){
                let newProduct1 = new Product(1, false, 'Yeti de compagnie', 2, 1);
                let newProduct2 = new Product(2, false, 'Alarme à fromage', 1, 1);
                let products = Array();
                products.push(newProduct1);
                products.push(newProduct2);
                let newList = new List(
                    false,
                    "M&M’s® World ",
                    new Date('2021-10-12').toLocaleDateString(),
                    false,
                    true,
                    1,
                    products
                );
                listService.dao.insert(newList).then(idReturned => {
                    newList.id = idReturned;
                    let newShare = new Sharing(newList, 1, 2, 'read');
                    sharingService.dao.insert(newShare);
                    let newShare2 = new Sharing(newList, 3, 2, 'write');
                    sharingService.dao.insert(newShare2);
                }).catch(e => reject(e));

                let newList2 = new List(
                    false,
                    "DisneyLand",
                    new Date('2021-07-21').toLocaleDateString(),
                    false,
                    true,
                    1
                );
                listService.dao.insert(newList2).then(idReturned => {
                    newList2.id = idReturned;
                    let newShare = new Sharing(newList2, 2, 1, 'read');
                    sharingService.dao.insert(newShare);
                    let newShare2 = new Sharing(newList2, 3, 1, 'write');
                    sharingService.dao.insert(newShare2);
                }).catch(e => reject(e));

                }, 6000);

        } catch (e) {
            console.log(e);
            if (e.code === "42P07") { // TABLE ALREADY EXISTS https://www.postgresql.org/docs/8.2/errcodes-appendix.html
                resolve()
            } else {
                reject(e)
            }
            return
        }

    })
}