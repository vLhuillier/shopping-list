const pg = require('pg')
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const morgan = require('morgan')

const ListService = require("./services/list")
const ProductService = require("./services/product")
const UserService = require("./services/user")
const SharingService = require("./services/sharing")

const app = express()
app.use(bodyParser.urlencoded({ extended: false })) // URLEncoded form data
app.use(bodyParser.json()) // application/json
app.use(cors())
app.use(morgan('dev')); // toutes les requêtes HTTP dans le log du serveur
app.use(cookieParser())

//const connectionString = "postgres://user:password@192.168.56.101/instance"
const connectionString = "postgres://shopping_list_adm:root@localhost/shopping_list"
const db = new pg.Pool({ connectionString: connectionString })
const listService = new ListService(db)
const productService = new ProductService(db)
const userService = new UserService(db)
const sharingService = new SharingService(db)
const jwt = require('./jwt')(userService)

require('./api/list')(app, listService, productService, jwt)
require('./api/product')(app, productService, jwt)
require('./api/user')(app, userService, jwt)
require('./api/sharing')(app, listService, userService, sharingService, jwt)
require('./datamodel/seeder')(listService, productService, userService, sharingService)
    .then(app.listen(3333))


