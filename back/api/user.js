const jwt2 = require('jsonwebtoken')
module.exports = (app, userService, jwt) => {

    /* Authentification */
    app.post('/user/authenticate', (req, res) => {
        const { email, password } = req.body

        if ((email === undefined) || (password === undefined)) {
            res.status(400).end();
            return;
        }
        userService.validatePassword(email, password)
            .then(authenticated => {
                if (!authenticated) {
                    res.status(401).end();
                    return;
                }
                res.json({'token': jwt.generateJWT(email)})
            })
            .catch(e => {
                res.status(500).end();
            });
    });

    /* Inscription */
    app.post('/user/register', (req, res) => {
        const { username, email, password } = req.body
        if ((username === undefined) || (email === undefined) || (password === undefined)) {
            res.status(400).end();
            return;
        }
        let dateNow = new Date().toLocaleDateString();
        userService.insert(username, email, password, dateNow)
            .then(user => {
                return res.json(user);
            }).catch(e => {
            res.status(500).end();
        });
    });

    /* Recupère l'utilisateur par l'email */
    app.get("/user/:email", async (req, res) => {
        try {
            const user = await userService.dao.getByEmail(req.params.email)
            if (user === undefined) {
                return res.status(404).end();
            }
            return res.json(user);
        } catch (e) { res.status(400).end() };
    });

    /* Vérifie l'utilisateur par l'email */
    app.get("/verifyUser/:email", async (req, res) => {
        try {
            const user = await userService.dao.getByEmailToUser(req.params.email)
            if (user === undefined) {
                return res.status(404).end();
            }
            return res.json(user);
        } catch (e) { res.status(400).end() };
    });

    /* Recherche un utilisateur par l'email et username */
    app.get("/searchUser/:query", async (req, res) => {
        try {
            const users = await userService.dao.searchByEmailAndUsername(req.params.query)
            return res.json(users);
        } catch (e) { res.status(400).end() };
    });

    /* Vérifie le token */
    app.get("/user/checkAuthentication/:token", jwt.validateJWT, async (req, res) => {
        try {
            const user = jwt.getUser();
            return res.json(user);
        } catch (e) {
            res.status(400).end()
        };
    });
}