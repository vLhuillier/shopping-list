class Constants{
    constructor() {

        /* Erreurs */
        this.HTTP_ERRORS_DEFAULT = "Une erreur s'est produite. Service injoignable ou problème réseau";

        this.ERROR_DEFAULT = "Une erreur est survenue. Si le problème persiste, veuillez contacter un administrateur.";

        this.HTTP_ERRORS = [
            {
                key: 0,
                text: 'Une erreur s\'est produite. Veuillez re-essayer ultérieurement.'
            },
            {
                key: 400,
                text: this.ERROR_DEFAULT
            },
            {
                key: 401,
                text: 'Non autorisé: Vous n\'êtes pas connecté. Veuillez-vous connecter.'
            },
            {
                key: 403,
                text: 'Erreur: Non autorisé. Veuillez re-essayer ultérieurement.'
            },
            {
                key: 404,
                text: 'Erreur: Donnée introuvable. Veuillez re-essayer ultérieurement.'
            },
            {
                key: 500,
                text: 'Erreur: Erreur interne du serveur. Veuillez re-essayer ultérieurement.'
            },
            {
                key: 503,
                text: 'Erreur: Le serveur est temporairement indisponible ou en maintenance. Veuillez re-essayer ultérieurement.'
            },
            {
                key: 504,
                text: 'Erreur: Le serveur ne répond pas. Veuillez re-essayer ultérieurement.'
            }
        ];

        /* Formulaire Liste */
        this.LIST_NAME_INPUT_SELECTOR =  document.getElementById('listNameInput');

        this.LIST_DATE_INPUT_SELECTOR =  document.getElementById('listDateInput');

        this.LIST_PRODUCT_CONTAINER_SELECTOR =  document.getElementById('listProductContainer');

        this.PRODUCT_NAME_INPUT_SELECTOR =  document.getElementById('productNameInput');

        this.PRODUCT_QUANTITY_INPUT_SELECTOR =  document.getElementById('productQuantityInput');

        this.PRODUCT_ADD_BUTTON_SELECTOR =  document.getElementById('productAddButton');

        this.LIST_ADD_BUTTON_SELECTOR = document.getElementById('listAddButton');

        this.LIST_ADD_FAB_BUTTON_SELECTOR = document.getElementById('listAddFabButton');

        /* Formulaire Liste Partagée */
        this.SHARED_PRODUCT_NAME_INPUT_SELECTOR =  document.getElementById('sharedProductNameInput');

        this.SHARED_LIST_NAME_INPUT_SELECTOR =  document.getElementById('sharedListNameInput');

        this.SHARED_LIST_DATE_INPUT_SELECTOR =  document.getElementById('sharedListDateInput');

        this.SHARED_LIST_PRODUCT_CONTAINER_SELECTOR =  document.getElementById('sharedListProductContainer');

        this.SHARED_PRODUCT_NAME_INPUT_SELECTOR =  document.getElementById('sharedProductNameInput');

        this.SHARED_PRODUCT_QUANTITY_INPUT_SELECTOR =  document.getElementById('sharedProductQuantityInput');

        this.SHARED_PRODUCT_ADD_BUTTON_SELECTOR =  document.getElementById('sharedProductAddButton');

        this.SHARED_LIST_ADD_BUTTON_SELECTOR = document.getElementById('sharedListAddButton');

        this.SHARED_LIST_ADD_FAB_BUTTON_SELECTOR = document.getElementById('sharedListAddFabButton');

        this.SHARED_LIST_USER_CONTAINER_SELECTOR =  document.getElementById('result_containerSharedUser');

        this.SHARED_LIST_USER_TBODY_CONTAINER_SELECTOR =  document.getElementById('sharedListUserContainer');

        this.SHARED_USER_NAME_INPUT_SELECTOR =  document.getElementById('sharedUserNameInput');

        this.SHARED_USER_PERMISSION_INPUT_SELECTOR = document.getElementById('sharedUserInput');



        /* Formulaire Liste Historique */
        this.LIST_DATE_RESTORE_INPUT_SELECTOR =  document.getElementById('restoreListDateInput');

        /* Formulaire Login */
        this.MODAL_LOGIN_SELECTOR = document.getElementById('modalLogin');
        this.LOGIN_FORM_SELECTOR = document.getElementById('login-page');
        this.REGISTER_FORM_SELECTOR = document.getElementById('register-page');

        /* Loader */
        this.LOADER_SELECTOR = document.getElementById('loader');





    }
}

