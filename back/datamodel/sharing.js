const BaseDAO = require('./list')
const List = require("./list");

module.exports = class Sharing {
    constructor(list = new List(), user_id, owner, permission) {
        this.list = list;
        this.user_id = user_id;
        this.owner = owner;
        this.permission = permission;
    }
    toString() {
        return `${this.list.id} ${this.user_id} ${this.permission}`
    }
}