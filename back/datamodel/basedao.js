const Product = require("../datamodel/product")

module.exports = class BaseDAO {

    constructor(db, tableList, tableProduct, tableSharing) {
        this.db = db;
        this.tableList = tableList;
        this.tableProduct = tableProduct;
        this.tableSharing = tableSharing;
        this.setProductsToLists();
    }

    setProductsToLists(lists, shared = false){
        if(lists !== undefined && lists !== null ){
            /* Supprime les listes en doublons, et stocke les produits de chaque liste dans un tableau */
            let products = Array();
            const seen = new Set();
            let newLists = lists.filter(el => {
                const duplicate = seen.has(el.id);
                seen.add(el.id);
                let product = new Product(el.product_id, el.product_checked, el.label, el.quantity, el.list_id);
                products.push(product);
                return !duplicate;
            });

            /* Affecte les produits des doublons aux listes */
            newLists.forEach(list => {
                if(typeof list.products === 'undefined' || list.products === null){
                    list.products = Array();
                }
                if(products.length > 0){
                    products.forEach(product => {
                        if((list.id ? list.id.toString() : true) === (product.list_id ? product.list_id.toString() : false)){
                            let foundId = products.find(function(item){
                                return item === list.id.toString();
                            });
                            if(foundId == null){
                                list.products.push(product);
                            }
                        }
                    });
                }
                /* Supprime les propriétées inutiles après traitement */
                delete list.product_id;
                delete list.product_checked;
                delete list.label;
                delete list.label;
                delete list.quantity;
                delete list.list_id;
            });
            return newLists;
        }
    }

}